-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: dragan_markovic
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Leskovac','16000'),(2,'Beograd','11000'),(3,'Nis','1000'),(4,'Zagreb','25000'),(5,'Portoroz','26000'),(6,'Albukerki','33000'),(7,'NewYork','25780'),(8,'London','22900'),(9,'Pitsburg','95000'),(10,'Waldorf','74000'),(11,'Moscow','65240'),(12,'Paris','33548'),(13,'Athen','24700'),(14,'Mislodjin','12180'),(15,'Istanbul','15151');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `exam`
--

LOCK TABLES `exam` WRITE;
/*!40000 ALTER TABLE `exam` DISABLE KEYS */;
INSERT INTO `exam` VALUES (1,'2015-12-12',1,1),(3,'2015-11-11',1,2),(4,'2015-07-08',3,1),(5,'2020-05-05',1,3);
/*!40000 ALTER TABLE `exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `professor`
--

LOCK TABLES `professor` WRITE;
/*!40000 ALTER TABLE `professor` DISABLE KEYS */;
INSERT INTO `professor` VALUES (1,'adres1','email@rrett','dragoon1','lastname','1212121212','2001-11-11',2,2),(3,'adres2','dm2005@wer.rir','dragan','markovic','1212121212','2001-11-05',2,2),(4,'adres3','email@rrettfg','mithoran','aslatrig','1234567890','2001-11-11',1,1),(6,'adres5','den1@den2','zorhan',NULL,'5555555555','2006-05-05',3,1),(7,'adres5','den3@den4.com','Paul',NULL,'4121412141','2016-05-04',2,1),(8,'adres6','den5@den4.com','Mitchel',NULL,'7848484848','2006-05-05',5,2),(9,'adres7','den7@den4.com','Dehan',NULL,'1414575577','2008-05-03',4,2),(10,'adrs8','den8@den4.com','Zaghor',NULL,'1111111111','2006-05-05',5,3);
/*!40000 ALTER TABLE `professor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (3,'rol3'),(1,'role1'),(2,'role2');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'adresa1',1,'ada@ert.lip','student 1','100','laastname','1234567890',1),(10,'adres2',2,'dragan.markovic@eng.it','student2','101','last12','1212121114',1),(18,'adres23',3,'bi@bert.com','student 3','105','laastname1','5555555555',3),(19,'adres34',2,'drkovic@eng.i','student 4','102','laastname2','4545454544',4),(20,'adres4',1,'birt@bert.com','student 5','103','laastname3','3232323232',3),(21,'address4',2,'draarkovic@eng.it','student 6','104','laastname',NULL,2);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `student_exams`
--

LOCK TABLES `student_exams` WRITE;
/*!40000 ALTER TABLE `student_exams` DISABLE KEYS */;
INSERT INTO `student_exams` VALUES (13,'2020-05-05',10,1),(14,NULL,18,4),(15,'2020-05-07',19,1),(16,NULL,10,3);
/*!40000 ALTER TABLE `student_exams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'description 1','math 1','WINTER',2),(2,'description 2','math 2','WINTER',1),(3,'description 33','math 3','WINTER',1),(4,'description 3','math 1','WINTER',2),(5,'desc5','phisic 4','WINTER',1);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `title`
--

LOCK TABLES `title` WRITE;
/*!40000 ALTER TABLE `title` DISABLE KEYS */;
INSERT INTO `title` VALUES (1,'t1'),(2,'t2'),(3,'t3');
/*!40000 ALTER TABLE `title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,'2018-05-05','user@user.com','1234',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-13  7:13:12
