function addProfessorToTable(professor) {
	$("#professors").children('tbody').append(
			'<tr><td></td>' + '<td>' + professor.firstname + '</td>' + '<td>'
					+ professor.lastname + '</td>'  + '</tr>');
}

function loadTable(page, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/professor/pages",
		  data: {"page": page, "num" : num},
		  type: "get",
		  success: function(data, s, r) {
			  $('#professors').children('tbody').html('');
			  for(var i = 0; i < data.length; i++){
				  
				  $('#professors').append('<tr><td>'+ data[i].id +'</td><td>'+ data[i].firstname +'</td><td>'+ data[i].lastname 
						  +'</td><td>'+ data[i].address +'</td><td>'+ data[i].title.name +'</td><td>'+ new Date(data[i].reelectiondate).toDateString() +'</td><td><button class="btn-outline-info pull-right" onclick=location.href="/DraganMarkovicFON/professor/'+data[i].id+'">Info</button>'
						  +'</td><td><button class="btn-outline-info pull-right"  onclick=location.href="/DraganMarkovicFON/professor/update/'+data[i].id+'">Update</button>'
						  +'</td><td><button class="btn-outline-danger pull-right" onclick=location.href="/DraganMarkovicFON/professor/delete/'+data[i].id+'">Delete</button>'
						  +'</td>'); 
				  }
		  },
		  error: function(status, xhr) {
			  alert(status);
		  }
		});
	createPagination(page, num);
}

function createPagination(current, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/professor/totalCount",
		  type: "get",
		  success: function(data, s, r) {
				$('ul.pagination').html("");
				var pages = Math.ceil(data/num);
				for(var i = 1; i <= pages; i++){
					$('ul.pagination').append('<li class="page-item"><a class="page-link" onclick="loadTable('+i+',' +$('#num select').val()+')">'+i+'</a></li>');
				}
				$('a[onclick^="loadTable('+current+',"]').parent().addClass('active');
		  },
		  error: function(status, xhr) {
		    	alert(status);
		  }
		});
}

function validateProfessor(){
	$(".is-invalid").removeClass('is-invalid');
	emptyField('#firstname');
	if (!$(".is-invalid").length) {
		return true;
	}
	return false;
}

function ajaxAddProfessor(professorForm,titleDto,cityDto){
	$.ajax({
		url : "/DraganMarkovicFON/api/professor",
		method : "post",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(professorDto,titleDto,cityDto),
		success : function(result) {
			addProfessorToTable(result);
			$("form")[0].reset();
			$('#professor').parent().parent().hide();
			$("#confirmation-modal").modal('hide');
		},
		error : function(result) {
			alert(result);
		}
	});
}

function ajaxFindProfessor(professorDto){
	$.ajax({
		url: "/DraganMarkovicFON/api/professor/findBy",
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(professorForm),
		success:function(data, s, r){
			$('#modal .modal-body dl').html(`<dt>Firstname</dt><dd>${data.firstname}</dd>`);
			$('#modal .modal-body dl').append(`<dt>Lastname</dt><dd>${data.lastname}</dd>`);
		$('#modal').modal('show');
		},
		error: function(){
			alert("Professor can't be shown!");
		}
	});
}