function addSubjectToTable(subject) {
	$("#subjects").children('tbody').append(
			'<tr><td></td>' + '<td>' + subject.name + '</td>' + '<td>'
					+ subject.description + '</td>'  + '</tr>');
}

function loadTable(page, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/subject/pages",
		  data: {"page": page, "num" : num},
		  type: "get",
		  success: function(data, s, r) {
			  $('#subjects').children('tbody').html('');
			  for(var i = 0; i < data.length; i++){
				  
				  $('#subjects').append('<tr><td>'+ data[i].id +'</td><td>'+ data[i].name +'</td><td>'+ data[i].description +'</td><td>'+ data[i].semester +'</td>'+'</tr>');
			  }
		  },
		  error: function(status, xhr) {
			  alert(status);
		  }
		});
	createPagination(page, num);
}

function createPagination(current, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/subject/totalCount",
		  type: "get",
		  success: function(data, s, r) {
				$('ul.pagination').html("");
				var pages = Math.ceil(data/num);
				for(var i = 1; i <= pages; i++){
					$('ul.pagination').append('<li class="page-item"><a class="page-link" onclick="loadTable('+i+',' +$('#num select').val()+')">'+i+'</a></li>');
				}
				$('a[onclick^="loadTable('+current+',"]').parent().addClass('active');
		  },
		  error: function(status, xhr) {
		    	alert(status);
		  }
		});
}

function validateSubject(){
	$(".is-invalid").removeClass('is-invalid');
	emptyField('#name');
	
	if (!$(".is-invalid").length) {
		return true;
	}
	return false;
}

function ajaxAddSubject(subjectDto){
	$.ajax({
		url : "/DraganMarkovicFON/api/subject",
		method : "post",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(subjectDto),
		success : function(result) {
			addSubjectToTable(result);
			$("form")[0].reset();
			$('#subject').parent().parent().hide();
			$("#confirmation-modal").modal('hide');
		},
		error : function(result) {
			alert("poruka"+result);
		}
	});
}

function ajaxFindSubject(subjectDto){
	$.ajax({
		url: "/DraganMarkovicFON/api/subject/findBy",
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(subjectDto),
		success:function(data, s, r){
			$('#modal .modal-body dl').html(`<dt>name</dt><dd>${data.name}</dd>`);
			$('#modal .modal-body dl').append(`<dt>description</dt><dd>${data.description}</dd>`);
			$('#modal .modal-body dl').append(`<dt>yearOfStudy</dt><dd>${data.yearOfStudy}</dd>`);
			$('#modal .modal-body dl').append(`<dt>semester</dt><dd>${data.semester}</dd>`);

		$('#modal').modal('show');
		},
		error: function(){
			alert("Subject can't be shown!");
		}
	});
}