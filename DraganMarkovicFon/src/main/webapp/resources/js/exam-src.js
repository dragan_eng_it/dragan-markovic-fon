
function addExamToTable(exam) {
	$("#exams").children('tbody').append(
			'<tr><td></td>' + '<td>' + exam.subjectDto + '</td>' + '<td>'
					+ exam.professorDto + '</td>'+ '<td>'
					+ exam.examdate + '</td>'  + '</tr>');
}

function loadTable(page, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/exam/pages",
		  data: {"page": page, "num" : num},
		  type: "get",
		  success: function(data, s, r) {
			  $('#exams').children('tbody').html('');
			  for(var i = 0; i < data.length; i++){
				  
				  $('#exams').append('<tr><td>'+ data[i].id +'</td><td>'+ data[i].subject.id +'o</td><td>'+ data[i].subject.name +'</td><td>'+ data[i].subject.yearOfStudy +'</td><td>'+ data[i].subject.semester +'</td><td>'+ data[i].professor.firstname+' '+data[i].professor.lastname+'</td><td>' +'</td><td>'+ new Date(data[i].examdate).toDateString() +'</td>'
						  +'<button type="button" class="btn btn-secondary" data-dismiss="modal"> Edit </button>'
						  +'<button type="button" class="btn btn-secondary" data-dismiss="modal">delete</button>'+'</tr>');
			  }
		  },
		  error: function(status, xhr) {
			  alert(status);
		  }
		});
	createPagination(page, num);
}

function createPagination(current, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/exam/totalCount",
		  type: "get",
		  success: function(data, s, r) {
				$('ul.pagination').html("");
				var pages = Math.ceil(data/num);
				for(var i = 1; i <= pages; i++){
					$('ul.pagination').append('<li class="page-item"><a class="page-link" onclick="loadTable('+i+',' +$('#num select').val()+')">'+i+'</a></li>');
				}
				$('a[onclick^="loadTable('+current+',"]').parent().addClass('active');
		  },
		  error: function(status, xhr) {
		    	alert(status);
		  }
		});
}

function validateExam(){
	$(".is-invalid").removeClass('is-invalid');
	emptyField('#examdate');
	if (!$(".is-invalid").length) {
		return true;
	}
	return false;
}
function findStudent(){
	if (!$("indexnum").length) {
		var studentDto=$("indexnum").val();
		ajaxFindStud(studentDto);
		return true;
	}
	return false;
}

function ajaxAddExam(examForm){
	$.ajax({
		url : "/DraganMarkovicFON/api/exam",
		method : "post",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(examForm),
		success : function(result) {
			addExamToTable(result);
			$("form")[0].reset();
			$('#exam').parent().parent().hide();
			$("#confirmation-modal").modal('hide');
		},
		error : function(result) {
			alert(result);
		}
	});
}

function ajaxPrijaviExam(examDto){
	$.ajax({
		url : "/DraganMarkovicFON/exam/prijava",
		method : "post",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(examDto),
		success : function(result) {
		},
		error : function(result) {
			alert(result);
		}
	});
}
function ajaxFindStud(sudentDto){
	$.ajax({
		url: "/DraganMarkovicFON/api/student/findBy",
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(studentDto),
		success:function(data, s, r){
			$('#stud').html(`<dt>Firstname</dt><dd>${data.firstname}</dd>`);
			$('#stud').append(`<dt>Lastname</dt><dd>${data.lastname}</dd>`);
		$('#modal').modal('show');
		},
		error: function(){
			alert("student can't be shown!");
		}
	});
}


function ajaxFindExam(examForm){
	$.ajax({
		url: "/DraganMarkovicFON/api/exam/findBy",
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(examForm),
		success:function(data, s, r){
			$('#modal .modal-body dl').html(`<dt>subject</dt><dd>${data.subject.name}</dd>`);
			$('#modal .modal-body dl').append(`<dt>professor</dt><dd>${data.professor.firstname}</dd>`);
			$('#modal .modal-body dl').append(`<dt>examdate</dt><dd>${ new Date(data.examdate).toDateString()}</dd>`);
			$('#modal .modal-body dl').append(`<dt>semester</dt><dd>${data.subject.semester}</dd>`);

			$('#modal .modal-body dl').append('<dt></button> Get student </button></dd>');

			$('#modal').modal('show');
		},
		error: function(){
			alert("Exam can't be shown!");
		}
	});
}