function loadComboboxCitiFromCountry(country) {
	$.ajax({
		url : "/DraganMarkovicFON/api/country/" + country, 
		method : "get",
		success : function(cities) {
			$('#city option').remove();
			if (cities.length)
				cities.forEach(function(x) {
					$('#city').append(new Option(x.name, x.id));
				});
		},
		error : function(result) {
			alert(result);
		}
	});
}

function loadComboboxCities() {
	$.ajax({
		url : "/DraganMarkovicFON/api/city",
		method : "get",
		success : function(cities) {
			$('#city option').remove();
			$('#city').append(new Option("Choose", ''));
			$('#city').val('');
			for (var i = 0; i < cities.length; i++) {
				$('#city').append(new Option(cities[i].name, cities[i].id));
			}

		},
		error : function(result) {
			alert("loadComboboxCities greska" + result);
		}
	});

}

function loadComboboxTitles() {
	$.ajax({
		url : "/DraganMarkovicFON/api/title",
		method : "get",
		success : function(titles) {
			$('#title option').remove();
			$('#title').append(new Option("Choose", ''));
			$('#title').val('');
			for (var i = 0; i < titles.length; i++) {
				$('#title').append(new Option(titles[i].name, titles[i].id));
			}

		},
		error : function(result) {
			alert("loadComboboxTitles greska" + result);
		}
	});

}

function emptyField(input) {
	if (!$(input).val()) {
		$(input).addClass('is-invalid');
	}
}
function loadComboboxSemester() {
	$.ajax({
		url : "/DraganMarkovicFON/api/subject/semester",
		method : "get",
		success : function(semesters) {
			$('#semester option').remove();
			$('#semester').append(new Option("Choose", ''));
			$('#semester').val('');
			for (var i = 0; i < semesters.length; i++) {
				$('#semester').append(new Option(semesters[i].name,semesters[i].name));
			}

		},
		error : function(result) {
			alert("loadComboboxsemesters error" + result);
		}
	});

}

