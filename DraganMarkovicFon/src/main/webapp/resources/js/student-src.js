

function addStudentToTable(data) {
	$('#students').append('<tr><td>'+ data[i].id +'</td><td>'+ data[i].indexnumber +'</td><td>'+ data[i].firstname +'</td><td>'+ data[i].lastname +'</td><td>'+ data[i].email +'</td><td>'+ data[i].phone 
			  +'</td><td>'+ data[i].city.name +'</td>'+'<td><button class="btn-outline-info pull-right" onclick=location.href="/DraganMarkovicFON/student/'+data[i].id+'">Info</button>'
			  +'</td><td><button class="btn-outline-info pull-right"  onclick=location.href="/DraganMarkovicFON/student/update/'+data[i].id+'">Update</button>'
			  +'</td><td><button class="btn-outline-danger pull-right" onclick=location.href="/DraganMarkovicFON/student/delete/'+data[i].id+'">Delete</button>'
			  +'</td>');
}
function loadTable(page, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/student/pages",
		  data: {"page": page, "num" : num},
		  type: "get",
		  success: function(data, s, r) {
			  $('#students').children('tbody').html('');
			  for(var i = 0; i < data.length; i++){
				  
				  $('#students').append('<tr><td>'+ data[i].indexnumber +'</td><td>'+ data[i].firstname +'</td><td>'+ data[i].lastname +'</td><td>'+ data[i].email +'</td><td>'+ data[i].phone 
						  +'</td><td>'+ data[i].city.name +'</td>'+'<td><button class="btn-outline-info pull-right" onclick=location.href="/DraganMarkovicFON/student/'+data[i].id+'">Info</button>'
						  +'</td><td><button class="btn-outline-info pull-right"  onclick=location.href="/DraganMarkovicFON/student/update/'+data[i].id+'">Update</button>'
						  +'</td><td><button class="btn-outline-danger pull-right" onclick=location.href="/DraganMarkovicFON/student/delete/'+data[i].id+'">Delete</button>'
						  +'</td>');
			  }
		  },
		  error: function(status, xhr) {
			  alert(status);
		  }
		});
	createPagination(page, num);
}

function createPagination(current, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/student/totalCount",
		  type: "get",
		  success: function(data, s, r) {
				$('ul.pagination').html("");
				var pages = Math.ceil(data/num);
				for(var i = 1; i <= pages; i++){
					$('ul.pagination').append('<li class="page-item"><a class="page-link" onclick="loadTable('+i+',' +$('#num select').val()+')">'+i+'</a></li>');
				}
				$('a[onclick^="loadTable('+current+',"]').parent().addClass('active');
		  },
		  error: function(status, xhr) {
		    	alert(status);
		  }
		});
}

function validateStudent(){
	$(".is-invalid").removeClass('is-invalid');
	emptyField('#firstname');
	if (!$(".is-invalid").length) {
		return true;
	}
	return false;
}

function ajaxAddStudent(studentDto){
	$.ajax({
		url : "/DraganMarkovicFON/api/student",
		method : "post",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(studentDto),
		success : function(result) {
			addStudentToTable(result);
			$("form")[0].reset();
			$('#student').parent().parent().hide();
			$("#confirmation-modal").modal('hide');
		},
		error : function(result) {
			alert(result);
		}
	});
}

function ajaxFindStudent(studentDto){
	$.ajax({
		url: "/DraganMarkovicFON/api/student/findBy",
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(studentDto),
		success:function(data, s, r){
			$('#modal .modal-body dl').html(`<dt>Firstname</dt><dd>${data.indexnum}</dd>`);
			$('#modal .modal-body dl').html(`<dt>Firstname</dt><dd>${data.firstname}</dd>`);
			$('#modal .modal-body dl').append(`<dt>Lastname</dt><dd>${data.lastname}</dd>`);
		$('#modal').modal('show');
		},
		error: function(){
			alert("student can't be shown!");
		}
	});
}