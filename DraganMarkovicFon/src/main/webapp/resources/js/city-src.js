function addCityToTable(city) {
	$("#cities").children('tbody').append(
			'<tr><td></td>' + '<td>' + city.number + '</td>' + '<td>'
					+ city.name + '</td>'  + '</tr>');
}

function loadTable(page, num){
	$.ajax({
		
		  url: "/DraganMarkovicFON/api/city/pages",
		  data: {"page": page, "num" : num},
		  type: "get",
		  success: function(data, s, r) {
			 
			  $('#cities').children('tbody').html('');
			  for(var i = 0; i < data.length; i++){
				  
				  $('#cities').append('<tr><td>'+ data[i].id +'</td><td>'+ data[i].number +'</td><td>'+ data[i].name +'</td></tr>');
			  };
			 
		  },
		  error: function(status, xhr) {
			  alert("<%=request.getContextPath()%>");
		  }
		});
	createPagination(page, num);
}

function createPagination(current, num){
	$.ajax({
		  url: "/DraganMarkovicFON/api/city/totalCount",
		  type: "get",
		  success: function(data, s, r) {
				$('ul.pagination').html("");
				var pages = Math.ceil(data/num);
				for(var i = 1; i <= pages; i++){
					$('ul.pagination').append('<li class="page-item"><a class="page-link" onclick="loadTable('+i+',' +$('#num select').val()+')">'+i+'</a></li>');
				}
				$('a[onclick^="loadTable('+current+',"]').parent().addClass('active');
		  },
		  error: function(status, xhr) {
		    	alert(status);
		  }
		});
}

function validateCity(){
	$(".is-invalid").removeClass('is-invalid');
	emptyField('#number');
	emptyField('#name');
	
	if (!$(".is-invalid").length) {
		return true;
	}
	return false;
}

function ajaxAddCity(cityDto){
	$.ajax({
		url : "/DraganMarkovicFON/api/city",
		method : "post",
		contentType : "application/json; charset=utf-8",
		data : JSON.stringify(cityDto),
		success : function(result) {
			addCityToTable(result);
			$("form")[0].reset();
			$('#city').parent().parent().hide();
			$("#confirmation-modal").modal('hide');
		},
		error : function(result) {
			alert(result);
		}
	});
}

function ajaxFindCity(cityDto){
	$.ajax({
		url: "/DraganMarkovicFON/api/city/findBy",
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(cityDto),
		success:function(data, s, r){
			$('#modal .modal-body dl').html(`<dt>number</dt><dd>${data.number}</dd>`);
			$('#modal .modal-body dl').append(`<dt>name</dt><dd>${data.name}</dd>`);
			$('#modal .modal-body dl').append('<td><button type="button" class="btn btn-secondary" data-dismiss="modal"> Edit </button>'+
			  '<button type="button" class="btn btn-secondary" data-dismiss="modal">delete</button></td>')
		$('#modal').modal('show');
		},
		error: function(){
			alert("City can't be shown!");
		}
	});
}