
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student-Exam Form</title>
</head>
<jsp:include page="../fragments/header.jsp" />

<body>

	<h1>Student exam's list</h1>

	<form:form name="StudentExam" id="enrollmentForm"
		action="submitEnrollment" modelAttribute="studentExam">
		<table cellspacing="0" cellpadding="0" border="0" width="718">
			
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Student:</td>
				<td><form:select path="studentDto">
						<form:option value="-" label="--Please Select" />
						<form:options items="${students}" itemValue="id"
							itemLabel="firstname" />
					</form:select></td>
			</tr>
			<tr>
				<td>Exams...:</td>
				<td><form:select path="examDto">
						<form:option value="-" label="--Please Select" />
						<form:options items="${examList}" itemValue="id"
							itemLabel="firstname" />
					</form:select></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Employee Last Name:</td>
				<td><form:input path="lastName" /> <form:errors
						path="lastName" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Employee Date for exam:</td>
				<td><form:input path="dateOfBirth" /> <form:errors
						path="dateOfBirth" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Place Of Birth:</td>
				<td><form:input path="placeofBirth" /> <form:errors
						path="placeofBirth" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit"
					id="submitButton" name="AddExam" value="AddExam" />
		</table>
	</form:form>



</body>
</html>