<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:include page="../fragments/header.jsp" />
<body data-gr-c-s-loaded="true">
	<div class="container">

				<jsp:include page="../fragments/printErrorsOnPage.jsp" />

		<c:choose>
			<c:when test="${professorDto['new']}">
				<h2>Add User</h2>
			</c:when>
			<c:otherwise>
				<h2>Update Professor</h2>
			</c:otherwise>
		</c:choose>
		<br />

		<spring:url value="/professors" var="professorActionUrl" />

		<form:form class="form-horizontal" method="post"
			modelAttribute="professorDto"
			action="${pageContext.request.contextPath}/professor/confirm">

			<form:hidden path="id" />

			<spring:bind path="firstname">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Name</label>
					<div class="col-sm-10">
						<form:input path="firstname" type="text" class="form-control "
							id="firstname" placeholder="firstname" required="true"
							minlength="3" maxlength="30" />
						<form:errors path="firstname" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="lastname">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">lastname</label>
					<div class="col-sm-10">
						<form:input path="lastname" type="text" class="form-control "
							id="lastname" placeholder="lastname" required="true"
							minlength="3" maxlength="30" />
						<form:errors path="lastname" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="address">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<form:textarea path="address" rows="1" class="form-control"
							id="address" placeholder="address" maxlength="50" />
						<form:errors path="address" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="email">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<form:input path="email" class="form-control" id="email"
							placeholder="email" required="true" maxlength="30" />
						<form:errors path="email" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="phone">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">phone</label>
					<div class="col-sm-10">
						<form:textarea path="phone" rows="1" class="form-control"
							id="phone" placeholder="phone" />
						<form:errors path="phone" class="control-label" />
					</div>
				</div>
			</spring:bind>
			<spring:bind path="reelectiondate">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">reelect.date ( dd/mm/yyyy )</label>
					<div class="col-sm-10">


						<form:textarea path="reelectiondate" rows="1" class="form-control"
							id="reelectiondate" placeholder="reelectiondate" required="true" />

						<form:errors path="reelectiondate" class="control-label" />
					</div>
				</div>
			</spring:bind>


			<spring:bind path="city">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">City</label>
					<div class="col-sm-5">
						<form:select path="city.id" class="form-control">
							<form:options items="${cities}" itemValue="id" itemLabel="name" />
						</form:select>
						<br />
						<form:errors path="city" class="control-label" />
					</div>
					<div class="col-sm-5"></div>
				</div>
			</spring:bind>
			<spring:bind path="title">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label"> title</label>
					<div class="col-sm-5">
						<form:select path="title.id" class="form-control">
							<form:options items="${titles}" itemValue="id" itemLabel="name" />
						</form:select>
						<br />
						<form:errors path="title" class="control-label" />
					</div>
					<div class="col-sm-5"></div>
				</div>
			</spring:bind>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<c:choose>
						<c:when test="${professorDto['new']}">
							<button type="submit" class="btn-lg btn-primary pull-right">Add</button>
						</c:when>
						<c:otherwise>
							<button type="submit" class="btn-lg btn-primary pull-right">Update</button>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</form:form>

	</div>
	<jsp:include page="../fragments/footer.jsp" />






	<script
		src="<%=request.getContextPath()%>/webjars/jquery/3.1.1/jquery.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/professor-src.js"></script>
	<script>
		$(document).ready(function() {
			//$.fn.datepicker.defaults.format = "dd.mm.yyyy";
			//loadComboboxCities();
			//loadComboboxTitles();
			$('#form-horizontal').validate({ // initialize the plugin
				rules : {
					name : {
						required : true,
						minlength : 3
					}
				},
				submitHandler : function(form) { // for demo
					alert('valid form submitted !!'); // for demo
					return false; // for demo
				}
			});

		});
		$(document).ready(function() {
			$('#btnSubmitForm').click(function() {
				if (validateProfessor()) {
					var professorDto = {
						firstname : $("#firstname").val(),
						lastname : $("#lastname").val(),
						address : $("#address").val(),
						email : $("#email").val(),
						phone : $("#phone").val(),
						reelectiondate : $("#reelectiondate").val(),
						cityDto : $("cityDto").val,
						titleDto : $("titleDto").val
					};

				}

				if (confirm("Are you sure?")) {
					ajaxAddProfessor(professorDto);
				}
			});

		});
	</script>


</body>
</html>