<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<%@ page isELIgnored="false"%>
<div class="container">

			<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<c:choose>
		<c:when test="${professorDto['new']}">
			<h2>To save this?</h2>
		</c:when>
		<c:otherwise>
			<h2>To update ?</h2>
		</c:otherwise>
	</c:choose>

	<br />
	<spring:url value="/professors" var="professorActionUrl" />

	<form:form class="form-horizontal" method="post"
		modelAttribute="professorDto"
		action="${pageContext.request.contextPath}/professor/save">


		<form:hidden path="id" readonly="true" />

		<spring:bind path="firstname">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<form:input path="firstname" type="text" class="form-control "
						id="firstname" placeholder="firstname" readonly="true" />
					<form:errors path="firstname" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="lastname">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">lastname</label>
				<div class="col-sm-10">
					<form:input path="lastname" type="text" class="form-control "
						id="lastname" placeholder="lastname" readonly="true" />
					<form:errors path="lastname" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="address">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Address</label>
				<div class="col-sm-10">
					<form:textarea path="address" rows="1" class="form-control"
						id="address" placeholder="address" readonly="true" />
					<form:errors path="address" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="email">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<form:input path="email" class="form-control" id="email"
						placeholder="Email" readonly="true" />
					<form:errors path="email" class="control-label" />
				</div>
			</div>
		</spring:bind>






		<spring:bind path="phone">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">phone</label>
				<div class="col-sm-10">
					<form:textarea path="phone" rows="1" class="form-control"
						id="phone" placeholder="phone" readonly="true" />
					<form:errors path="phone" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="reelectiondate">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">reelect.date</label>
				<div class="col-sm-10">


					<form:textarea path="reelectiondate" rows="1" class="form-control"
						id="reelectiondate" placeholder="reelectiondate" readonly="true" />

					<form:errors path="reelectiondate" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="city">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">City</label>
				<div class="col-sm-5">
					<form:select path="city.id" class="form-control">
						<form:options items="${cities}" itemValue="id" itemLabel="name" />
					</form:select>
					<br />
					<form:errors path="city" class="control-label" />
				</div>
				<div class="col-sm-5"></div>
			</div>
		</spring:bind>


		<spring:bind path="title">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label"> title</label>
				<div class="col-sm-5">
					<form:select path="title.id" class="form-control">
						<form:options items="${titles}" itemValue="id" itemLabel="name" />
					</form:select>
					<br />
					<form:errors path="title" class="control-label" />
				</div>
				<div class="col-sm-5"></div>
			</div>
		</spring:bind>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${professorDto['new']}">
						<td><button class="btn-lg btn-primary pull-right"
								id="cancell" name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="save"
								name="action" value="save">Save</button></td>
					</c:when>
					<c:otherwise>

						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="update"
								name="action" value="update">Update</button></td>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>