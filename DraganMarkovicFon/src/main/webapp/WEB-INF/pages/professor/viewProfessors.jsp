<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
	<!-- Page Content -->
	<div class="container fill">
		<jsp:include page="../fragments/printErrorsOnPage.jsp" />

		<div class="row">

			<h3>List of all professors..</h3>
			<div class="row">
				<jsp:include page="../fragments/tableForView.jsp" />

				<div class="col-sm-12 col-md-6" style="">
					<spring:url value="/professor/add" var="professorAdd" />
					<button class="btn-lg btn-primary right"
						onclick="location.href='${professorAdd}'">Add professor</button>
				</div>
			</div>
			<table class="table table-striped" id="professors">
				<thead>
					<tr>
						<th>#</th>
						<th>firstname</th>
						<th>lastname</th>
						<th>address</th>
						<th>title</th>
						<th>reelection_date</th>
						<th>info</th>
						<th>update</th>
						<th>
							<button type="button" class="btn btn-default btn-sm">
								<span class="glyphicon glyphicon-trash"></span> Trash
							</button>
						</th>


					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
			<nav>
				<ul class="pagination justify-content-center"
					style="flex-wrap: wrap;">
				</ul>
			</nav>
		</div>
	</div>
	<script type="text/javascript" src="${contextPath}/resources/js/src.js"></script>
	<script type="text/javascript"
		src="${contextPath}/resources/js/professor-src.js"></script>

	<jsp:include page="../fragments/footer.jsp" />
	<script>
		$(document).ready(function() {
			loadTable(1, $('#num select').val());

			$('#num select').change(function(e) {
				loadTable(1, $('#num select').val());
			});

		});
	</script>
</body>
</html>