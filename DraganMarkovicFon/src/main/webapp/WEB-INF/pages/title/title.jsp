<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Customer Save Page</title>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
</head>
<jsp:include page="../fragments/header.jsp" />

<body>
					<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<form:form method="POST" commandName="professor"
		action="professor">
		<table>
			<tr>
				<td>Name:</td>
				<td><form:input path="firstname" /></td>
				<td><form:errors path="firstname" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><form:input path="lastname" /></td>
				<td><form:errors path="lastname" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><form:input path="address" /></td>
				<td><form:errors path="address" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Phone:</td>
				<td><form:input path="phone" /></td>
				<td><form:errors path="phone" cssClass="error" /></td>
			</tr>
			<tr>
				<td>reelection date:</td>
				<td><form:input path="reelectiondate" placeholder="dd/MM/yyyy"/></td>
				<td><form:errors path="reelectiondate" cssClass="error" /></td>
			</tr>
			<tr>
				<td>Title:</td>
				<td><form:select path="title">
						<form:option value="" label="Select Title" />
						<form:option value="MALE" label="Male" />
						<form:option value="FEMALE" label="Female" />
					</form:select></td>
				<td><form:errors path="title" cssClass="error" /></td>
			</tr>
			<tr>
				<td>City:</td>
				<td><form:select path="city">
						<form:option value="" label="Select city" />
						<form:option value="1" label="city1" />
						<form:option value="2" label="city2" />
					</form:select></td>
				<td><form:errors path="city" cssClass="error" /></td>
			</tr>
			
			
			
			
			<tr>
				<td colspan="3"><input type="submit" value="Save Professor"></td>
			</tr>
		</table>

	</form:form>

</body>
</html>
