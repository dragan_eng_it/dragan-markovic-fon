<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
	<!-- Page Content -->
	<div class="container fill">
		<jsp:include page="../fragments/printErrorsOnPage.jsp" />

		<div class="row">
			<h3>List of all subjects..</h3>
			<div class="row">
				<jsp:include page="../fragments/tableForView.jsp" />
				<div class="col-sm-12 col-md-6" style="">
					<spring:url value="/subject/add" var="subjectAdd" />
					<button class="btn-lg btn-primary right"
						onclick="location.href='${subjectAdd}'">Add subject</button>
				</div>
			</div>


			<table class="table table-hover" id="subjects">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">name</th>
						<th scope="col">description</th>
						<th scope="col">semester</th>
						<th scope="col">yearOfStudy</th>
					</tr>
				</thead>
				<tbody>

				</tbody>
			</table>
			<nav>
				<ul class="pagination justify-content-center"
					style="flex-wrap: wrap;">
				</ul>
			</nav>
		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />

	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/subject-src.js"></script>

	<script>
		$(document).ready(function() {
			loadTable(1, $('#num select').val());

			$('#num select').change(function(e) {
				loadTable(1, $('#num select').val());
			});

			$('#subjects').on('click', 'tbody tr', function(e) {
				$('#modal .modal-title').html("Subject");
				var emp = {
					name : this.cells[1].innerHTML,
					description : this.cells[2].innerHTML
				}
				ajaxFindSubject(emp);
			});

		});
	</script>


</body>
</html>