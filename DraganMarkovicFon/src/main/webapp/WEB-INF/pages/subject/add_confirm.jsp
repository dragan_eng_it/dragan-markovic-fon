<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<%@ page isELIgnored="false"%>
<div class="container">
	<c:choose>
		<c:when test="${subjectForm['new']}">
			<h1>confirm new </h1>
		</c:when>
		<c:otherwise>
			<h1>confirm Update </h1>
		</c:otherwise>
	</c:choose>
	<br />

	<spring:url value="/subjects" var="subjectActionUrl" />

	<form:form class="form-horizontal" method="post"
			modelAttribute="subjectForm" action="/DraganMarkovicFON/subject/save">

		<form:hidden path="id" />
		<spring:bind path="name">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">name</label>
				<div class="col-sm-10">
					<form:input path="name" type="text" class="form-control " id="name"
						placeholder="name" />
					<form:errors path="name" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="description">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">description</label>
				<div class="col-sm-10">
					<form:input path="description" type="text" class="form-control "
						id="firstname" placeholder="description" />
					<form:errors path="description" class="control-label" />
				</div>
			</div>

		</spring:bind>

		<spring:bind path="yearOfStudy">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Year of st.</label>
				<div class="col-sm-10">
					<form:radiobuttons path="yearOfStudy" items="${numberList}"
						element="label class='radio-inline'" />
					<br />
					<form:errors path="yearOfStudy" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="semester">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">semester</label>
				<div class="col-sm-10">
					<form:select path="semester">
						<form:options items="${semesters}" itemValue="semester" itemLabel="semester" />
					</form:select>
					<br />
					<form:errors path="semester" class="control-label" />
				</div>
			</div>
		</spring:bind>



		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${subjectForm['new']}">
						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="save"
								name="action" value="save">Save</button></td>
					</c:when>
					<c:otherwise>

						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="update"
								name="action" value="update">Save</button></td>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>

</div>




<footer class="py-5 bg-dark">
	<div class="container">
		<p class="m-0 text-center text-white"></p>
	</div>
</footer>

<jsp:include page="../fragments/footer.jsp" />

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/src.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/subject-src.js"></script>
<script>
	
</script>


</body>
</html>