<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
					<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<div class="container fill">
		<div class="row">
			<div class="col-md-8">
				<br>
				<h3>
					<a href="addCity">New City</a>
				</h3>

				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Cities</h2>
						<div class="row">
												<jsp:include page="../fragments/tableForView.jsp" />

						</div>
						<table class="table table-hover" id="cities">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">number</th>
									<th scope="col">name</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
						<nav>
							<ul class="pagination justify-content-center"
								style="flex-wrap: wrap;">
							</ul>
						</nav>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Search</h5>
					<div class="card-body">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Search for..."> <span
								class="input-group-btn">
								<button class="btn btn-secondary" type="button">Go!</button>
							</span>
						</div>
					</div>
				</div>
				<!-- Categories Widget -->
				<div class="card my-4">
					<h5 class="card-header">Categories</h5>
					<div class="card-body">
						<div class="row"></div>
					</div>


				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../fragments/footer.jsp" />
	<script type="text/javascript" src="${contextPath}/resources/js/src.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/js/city-src.js"></script>
<script>
	$(document).ready(function() {
		loadTable(1, $('#num select').val());

		$('#num select').change(function(e) {
			loadTable(1, $('#num select').val());
		});

		$('#cities').on('click', 'tbody tr', function(e) {
			$('#modal .modal-title').html("CityDto");
			var emp = {
				number : this.cells[1].innerHTML,
				name : this.cells[2].innerHTML
			}
			ajaxFindCity(emp);
		});

	});
</script>
</body>
</html>