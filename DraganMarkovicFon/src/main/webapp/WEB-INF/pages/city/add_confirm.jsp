<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<%@ page isELIgnored="false"%>
<div class="container">
					<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<c:choose>
		<c:when test="${cityForm['new']}">
			<h1>Add city</h1>
		</c:when>
		<c:otherwise>
			<h1>Update city</h1>
		</c:otherwise>
	</c:choose>

	<br />

	<spring:url value="/cities" var="cityActionUrl" />

	<form:form class="form-horizontal" method="post"
		modelAttribute="cityForm" action="${pageContext.request.contextPath}/city/save">

		<form:hidden path="id" />

		<spring:bind path="name">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">name</label>
				<div class="col-sm-10">
					<form:input path="name" type="text" class="form-control "
						id="firstname" placeholder="name" />
					<form:errors path="name" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="number">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">number</label>
				<div class="col-sm-10">
					<form:input path="number" type="text" class="form-control "
						id="number" placeholder="number" />
					<form:errors path="number" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${cityForm['new']}">
						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="save"
								name="action" value="save">Save</button></td>
					</c:when>
					<c:otherwise>

						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="update"
								name="action" value="update">Save</button></td>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>

</div>


</div>

<jsp:include page="../fragments/footer.jsp" />





<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/src.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/resources/js/city-src.js"></script>
<script>
	
</script>


</body>
</html>