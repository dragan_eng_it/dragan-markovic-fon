<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<%@ page isELIgnored="false"%>
<div class="container">
	<c:choose>
		<c:when test="${cityForm['new']}">
			<h1>Add city</h1>
		</c:when>
		<c:otherwise>
			<h1>Update city</h1>
		</c:otherwise>
	</c:choose>

	<br />
						<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<spring:url value="/cities" var="cityActionUrl" />

	<form:form class="form-horizontal" method="post"
		modelAttribute="cityForm" action="${pageContext.request.contextPath}/city/confirm">
		
		<form:errors path="*" cssClass="errorblock" element="div" />
		<form:hidden path="id" />

		<spring:bind path="name">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">name</label>
				<div class="col-sm-10">
					<form:input path="name" type="text" class="form-control "
						id="firstname" placeholder="name" />
					<form:errors path="name" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<spring:bind path="number">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">number</label>
				<div class="col-sm-10">
					<form:input path="number" type="text" class="form-control "
						id="number" placeholder="number" />
					<form:errors path="number" class="control-label" />
				</div>
			</div>
		</spring:bind>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${cityForm['new']}">
						<button type="submit" class="btn-lg btn-primary pull-right">Add</button>
					</c:when>
					<c:otherwise>
						<button type="submit" class="btn-lg btn-primary pull-right">Update</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>

</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>