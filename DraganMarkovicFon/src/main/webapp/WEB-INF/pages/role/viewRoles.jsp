
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Blog Home - Start Bootstrap Template</title>
<!-- Bootstrap core CSS -->
<link rel='stylesheet'
	href='<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
<link rel='stylesheet' href='<%=request.getContextPath()%>/resources/css/style.css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
						<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<!-- Page Content -->
	<div class="container fill">
		<div class="row">
			<div class="col-md-8">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Roles</h2>
						<div class="row">
							<div class="col-sm-12 col-md-6" style="">
								<div id="num">
									<label style="display: flex; align-items: center;">Show
										<select name="num" class="form-control form-control-sm"
										style="margin: 0 1em;">
											<option value="10">10</option>
											<option value="15">15</option>
											<option value="20">20</option>
											<option value="50">50</option>
									</select> entries
									</label>
								</div>
							</div>
						</div>
						<table class="table table-hover" id="employees">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">name</th>
									<th scope="col">opis</th>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
						<nav>
							<ul class="pagination justify-content-center"
								style="flex-wrap: wrap;">
							</ul>
						</nav>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">Search</h5>
					<div class="card-body">
						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="Search for..."> <span
								class="input-group-btn">
								<button class="btn btn-secondary" type="button">Go!</button>
							</span>
						</div>
					</div>
				</div>
				<!-- Categories Widget -->
				<div class="card my-4">
					<h5 class="card-header">Categories</h5>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">Web Design</a></li>
									<li><a href="#">HTML</a></li>
									<li><a href="#">Freebies</a></li>
								</ul>
							</div>
							<div class="col-lg-6">
								<ul class="list-unstyled mb-0">
									<li><a href="#">JavaScript</a></li>
									<li><a href="#">CSS</a></li>
									<li><a href="#">Tutorials</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="card my-4">
						<h5 class="card-header">Side Widget</h5>
						<div class="card-body">You can put anything you want inside
							of these side widgets. They are easy to use, and feature the new
							Bootstrap 4 card containers!</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- Sidebar Widgets Column -->
	<script
		src="<%=request.getContextPath()%>/webjars/jquery/3.1.1/jquery.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
		<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/role-src.js"></script>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright © Your Website
				2019</p>
		</div>
	</footer>
	<script>
		$(document).ready(function() {
			loadTable(1, $('#num select').val());

			$('#num select').change(function(e) {
				loadTable(1, $('#num select').val());
			});

			$('#employees').on('click', 'tbody tr', function(e) {
				$('#modal .modal-title').html("RoleDto");
				var emp = {firstname: this.cells[1].innerHTML, lastname: this.cells[2].innerHTML }
				ajaxFindEmployee(emp);
			});
			
		});
	</script>

	<div class="modal" tabindex="-1" role="dialog" id="modal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"></h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">
					<dl>
					</dl>
					
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
			</div>
		</div>
		</div>
</body>
</html>