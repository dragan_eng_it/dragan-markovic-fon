<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Blog Home - Start Bootstrap Template</title>
<!-- Bootstrap core CSS -->
<link rel='stylesheet'
	href='<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
<link rel='stylesheet' href='<%=request.getContextPath()%>/resources/css/style.css'>
</head>
<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
					<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<div class="container fill">
		<div class="row">
			<div class="col-md-7">
				<br>
				<div class="card mb-4">
					<div class="card-body">
						<h2 class="card-title">Add Role</h2>
						<form class="form-horizontal">
							<fieldset>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="firstname">First
										name: </label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="firstname"
											required="">
										<div class="invalid-feedback">First name is invalid!</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="lastname">Last
										Name: </label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="lastname"
											required="">
										<div class="invalid-feedback">Last name is invalid!</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="country">Country</label>
									<div class="col-sm-8">
										<select id="country" name="country" class="form-control">
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="city">City</label>
									<div class="col-sm-8">
										<select id="city" name="city" class="form-control">
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-3 col-form-label" for="phonenumber">Phone
										number: </label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="phonenumber"
											required="">
										<div class="invalid-feedback">Phone number is invalid!</div>
									</div>
								</div>

							</fieldset>
						</form>

					</div>
					<div class="form-group">
						<div class="col-md-4">
							<button id="btnSubmitForm" name="submitForm"
								class="btn btn-primary">Save</button>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-5">
				<!-- Search Widget -->
				<div class="card my-4">
					<h5 class="card-header">New employees</h5>
					<div class="card-body">
						<div class="row">
							<table class="table" id="employees">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">First name</th>
										<th scope="col">Last name</th>
										<th scope="col">Country</th>
										<th scope="col">City</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright © Your Website
				2019</p>
		</div>
	</footer>

	<script
		src="<%=request.getContextPath()%>/webjars/jquery/3.1.1/jquery.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/role-src.js"></script>
		<script>
		$(document).ready(function() {
			$('#city').parent().parent().hide();
			loadComboboxCountries();

			$('#country').change(function() {
				if ($('#city').parent().parent().is(":hidden")) {
					$('#city').parent().parent().slideDown();
				}
				loadComboboxCities($('#country').val());
			});

			$('#btnSubmitForm').click(function() {
				if (validateEmployee()) {
					var employee = {
						firstname : $("#firstname").val(),
						lastname : $("#lastname").val(),
						cityDto : {
							number : $("#city").val()
						}
					}
					if (confirm("Are you sure?")) {
						ajaxAddEmployee(employee);
					}
				}
			});

		});
	</script>


</body>
</html>