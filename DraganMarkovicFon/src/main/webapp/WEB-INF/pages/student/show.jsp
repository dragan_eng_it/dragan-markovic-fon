<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<div class="container">
	<form:form name="exam" id="examForm" method="post"
		action="history.back()" modelAttribute="studentForm">


				<jsp:include page="../fragments/printErrorsOnPage.jsp" />


		<h1>Details for student </h1>
		<br />

		<div class="row">
			<label class="col-sm-2">ID</label>
			<div class="col-sm-10">${studentForm.id}</div>
		</div>
		<div class="row">
			<label class="col-sm-2">indexnumber</label>
			<div class="col-sm-10">${studentForm.indexnumber}</div>
		</div>
		<div class="row">
			<label class="col-sm-2">Name</label>
			<div class="col-sm-10">${studentForm.firstname}</div>
		</div>
		<div class="row">
			<label class="col-sm-2">lastname</label>
			<div class="col-sm-10">${studentForm.lastname}</div>
		</div>
		<div class="row">
			<label class="col-sm-2">Address</label>
			<div class="col-sm-10">${studentForm.address}</div>
		</div>
		<div class="row">
			<label class="col-sm-2">Email</label>
			<div class="col-sm-10">${studentForm.email}</div>
		</div>

		<div class="row">
			<label class="col-sm-2">phone</label>
			<div class="col-sm-10">${studentForm.phone}</div>
		</div>

		<div class="row">
			<label class="col-sm-2">Year of st.</label>
			<div class="col-sm-10">${studentForm.currentYearOfStudy}</div>
		</div>

		<div class="row">
			<label class="col-sm-2">City</label>
			<div class="col-sm-10">${studentForm.city}</div>
		</div>
		<div class="row">
			<button type="button" class="btn-lg btn-primary right" name="back" onclick="history.back()">back</button>
		</div>
		</form:form>
		
</div>

<footer> </footer>
<jsp:include page="../fragments/footer.jsp" />

</body>
</html>