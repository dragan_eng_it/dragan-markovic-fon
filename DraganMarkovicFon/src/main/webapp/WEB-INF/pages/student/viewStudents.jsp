<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<title>City view</title>
<jsp:include page="../fragments/header.jsp" />
<body>

	<div class="container">

		<jsp:include page="../fragments/printErrorsOnPage.jsp" />

		<h2>List of all students..</h2>
		<div class="row">
			<div class="col-sm-12 col-md-6" style="">
			<spring:url value="/student/add" var="studentAdd" />
						<button class="btn-lg btn-primary right"
							onclick="location.href='${studentAdd}'">Add student</button>
				</div>
		</div>
					<jsp:include page="../fragments/tableForView.jsp" />

		<table class="table table-striped" id="students">
			<thead>
				<tr>
					<th>#</th>
					<th>name</th>
					<th>lastname</th>
					<th>Email</th>
					<th>phone</th>
					<th>city</th>
					<th>info</th>
					<th>update</th>
					<th>
						<button type="button" class="btn btn-default btn-sm">
							<span class="glyphicon glyphicon-trash"></span> Trash
						</button>
					</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		<nav>
			<ul class="pagination justify-content-center"
				style="flex-wrap: wrap;">
			</ul>
		</nav>
	</div>
	
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/student-src.js"></script>

	<jsp:include page="../fragments/footer.jsp" />
	<script>
		$(document).ready(function() {
			loadTable(1, $('#num select').val());

			$('#num select').change(function(e) {
				loadTable(1, $('#num select').val());
			});

			$('#students').on('click', 'tbody tr', function(e) {
				$('#modal .modal-title').html("Student");
				var emp = {
					indexnumber : this.cells[1].innerHTML,
					name : this.cells[2].innerHTML
				}
				ajaxFindStudent(emp);
			});

		});
	</script>
</body>
</html>