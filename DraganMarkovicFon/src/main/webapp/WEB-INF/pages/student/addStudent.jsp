<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />
<%@ page isELIgnored="false"%>
<body>

	<div class="container">

				<jsp:include page="../fragments/printErrorsOnPage.jsp" />

		<c:choose>
			<c:when test="${studentForm['new']}">
				<h1>Add Student</h1>
			</c:when>
			<c:otherwise>
				<h1>Update student</h1>
			</c:otherwise>
		</c:choose>

		<br />

		<spring:url value="/students" var="studentActionUrl" />

		<form:form class="form-horizontal" method="post"
			modelAttribute="studentForm"
			action="${pageContext.request.contextPath}/student/confirm">

			<form:hidden path="id" />
			<c:choose>
				<c:when test="${studentForm['new']}">
					<spring:bind path="indexnumber">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<label class="col-sm-2 control-label">Index num</label>
							<div class="col-sm-10">
								<form:input path="indexnumber" type="text" class="form-control "
									id="indexnumber" placeholder="indexnumber" required="true"
									minlength="3" maxlength="10" />
								<form:errors path="indexnumber" class="control-label" />
							</div>
						</div>
					</spring:bind>
				</c:when>
				<c:otherwise>
					<spring:bind path="indexnumber">
						<div class="form-group ${status.error ? 'has-error' : ''}">
							<label class="col-sm-2 control-label">Index num</label>
							<div class="col-sm-10">
								<form:input path="indexnumber" type="text" class="form-control "
									id="indexnumber" placeholder="indexnumber" readonly="true" />
								<form:errors path="indexnumber" class="control-label" />
							</div>
						</div>
					</spring:bind>
				</c:otherwise>
			</c:choose>

			<spring:bind path="firstname">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">firstname</label>
					<div class="col-sm-10">
						<form:input path="firstname" type="text" class="form-control "
							id="firstname" placeholder="firstname" required="true"
							minlength="3" maxlength="30" />
						<form:errors path="firstname" class="control-label" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="lastname">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">lastname</label>
					<div class="col-sm-10">
						<form:input path="lastname" type="text" class="form-control "
							id="lastname" placeholder="lastname" required="true"
							minlength="3" maxlength="30" />
						<form:errors path="lastname" class="control-label" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="address">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Address</label>
					<div class="col-sm-10">
						<form:textarea path="address" rows="1" class="form-control"
							id="address" placeholder="address" maxlength="50" />
						<form:errors path="address" class="control-label" />
					</div>
				</div>
			</spring:bind>


			<spring:bind path="email">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<form:input path="email" class="form-control" id="email"
							placeholder="email" maxlength="30" />
						<form:errors path="email" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<spring:bind path="phone">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<p class="col-sm-2 control-label">phone</p>
					<div class="col-sm-10">
						<form:textarea path="phone" rows="1" class="form-control"
							id="phone" placeholder="phone" />
						<form:errors path="phone" class="control-label" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="currentYearOfStudy">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">Year of st.</label>
					<div class="col-sm-10">
						<form:radiobuttons path="currentYearOfStudy" items="${numberList}"
							element="label class='radio-inline'" />
						<br />
						<form:errors path="currentYearOfStudy" class="control-label" />
					</div>
				</div>
			</spring:bind>
			
			<spring:bind path="city">
				<div class="form-group ${status.error ? 'has-error' : ''}">
					<label class="col-sm-2 control-label">City</label>
					<div class="col-sm-10">
						<form:select path="city.id">
							<form:options items="${cities}" itemValue="id" itemLabel="name" />
						</form:select>
						<br />
						<form:errors path="city" class="control-label" />
					</div>
				</div>
			</spring:bind>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<c:choose>
						<c:when test="${studentForm['new']}">
							<div class="row">
								<button type="submit" class="btn-lg btn-primary">Add</button>

								<button type="button" class="btn-outline-info" name="back"
									onclick="history.back()">back</button>
							</div>

						</c:when>
						<c:otherwise>
							<div class="row">
								<button type="submit" class="btn-lg btn-primary ">Update</button>

								<button type="button" class="btn-outline-info " name="back"
									onclick="history.back()">back</button>
							</div>

						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</form:form>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/resources/js/src.js"></script>
		<script type="text/javascript"
			src="<%=request.getContextPath()%>/resources/js/student-src.js"></script>


	</div>

	<jsp:include page="../fragments/footer.jsp" />

</body>