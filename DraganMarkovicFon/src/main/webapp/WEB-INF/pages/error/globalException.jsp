<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My global exception</title>
</head>
<jsp:include page="../fragments/header.jsp" />

<body>
<h1>Error Page</h1>
	<button type="button" name="back" onclick="history.back()">back</button>
	
	<p>
		Error object: ${errorObj}
	</p>
	<p>
		Error message: ${errorMessage}
	</p>
	<h2>My global exception page....</h2>
	<p>Application has encountered an error. Please contact support on ...</p>


	<p>Failed URL: ${url} </p>
	
	<p>Exception: ${exception.message}</p>


</body>
</html>