<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
	<!-- Page Content -->

					<jsp:include page="../fragments/printErrorsOnPage.jsp" />
	<c:choose>
		<c:when test="${examDto['new']}">
			<h1>Add exam</h1>
		</c:when>
		<c:otherwise>
			<h1>Update exam</h1>
		</c:otherwise>
	</c:choose>
	<br />

	<form:form name="exam" id="examForm" method="post"
		action="${pageContext.request.contextPath}/exam/confirm" modelAttribute="examDto">


		<table cellspacing="0" cellpadding="0" border="0" width="718">
			<tr>
				<td>on date:</td>
				<td><form:input path="examdate" /> <form:errors
						path="examdate" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<spring:bind path="professor">

					<form:select path="professor.id">
						<c:forEach items="${professors}" var="professor">
							<form:option value="${professor.id}">${professor}</form:option>
						</c:forEach>
					</form:select>
				</spring:bind>

			</tr>
			<tr>
				<spring:bind path="subject.id">

					<form:select path="subject.id">
						<c:forEach items="${subjects}" var="subject">
							<form:option value="${subject.id}">${subject}</form:option>
						</c:forEach>
					</form:select>
				</spring:bind>

			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" id="save"
					name="save" value="save" />
		</table>

	</form:form>


<jsp:include page="../fragments/footer.jsp" />

</body>
</html>

</body>
</html>