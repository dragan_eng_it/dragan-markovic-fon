<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">
	<!-- Page Content -->

	<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<c:choose>
		<c:when test="${examDto['new']}">
			<h1>ok to Add exam?</h1>
		</c:when>
		<c:otherwise>
			<h1>ok to Update exam?</h1>
		</c:otherwise>
	</c:choose>
	<br />


	<form:form name="exam" id="examForm" method="post"
		action="${pageContext.request.contextPath}/exam/save" modelAttribute="examDto">

		<form:hidden path="id" />
		<table cellspacing="0" cellpadding="0" border="0" width="718">

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Exam Name:</td>
				<td><form:textarea path="subject" /> <form:errors
						path="subject" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Employee Last Name:</td>
				<td><form:textarea path="professor" /> <form:errors
						path="professor" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Employee Date Of Birth:</td>
				<td><form:textarea path="examdate" /> <form:errors
						path="examdate" cssStyle="color:red" /></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

		</table>


		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${subjectForm['new']}">
						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="save"
								name="action" value="save">Save</button></td>
					</c:when>
					<c:otherwise>

						<td><button class="btn-lg btn-primary pull-right" id="cancel"
								name="action" value="cancel">Cancel</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="change"
								name="action" value="change">Change</button></td>
						<td><button class="btn-lg btn-primary pull-right" id="update"
								name="action" value="update">Save</button></td>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>



<jsp:include page="../fragments/footer.jsp" />

	
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/subject-src.js"></script>
	<script>
		
	</script>


</body>
</html>