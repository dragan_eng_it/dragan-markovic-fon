<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<jsp:include page="../fragments/header.jsp" />

<div class="container">

			<jsp:include page="../fragments/printErrorsOnPage.jsp" />

	<h1>Detail</h1>
	<br />

	<div class="row">
		<label class="col-sm-2">ID</label>
		<div class="col-sm-10">${cityForm.id}</div>
	</div>
	
	<div class="row">
		<label class="col-sm-2">Name</label>
		<div class="col-sm-10">${cityForm.name}</div>
	</div>
	<div class="row">
		<label class="col-sm-2">number</label>
		<div class="col-sm-10">${cityForm.number}</div>
	</div>
	
<button type="button" name="back" onclick="history.back()">back</button>
</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>