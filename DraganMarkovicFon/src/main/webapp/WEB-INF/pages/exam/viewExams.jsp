<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:include page="../fragments/header.jsp" />

<body data-gr-c-s-loaded="true">

	<!-- Page Content -->
	<div class="container fill">
		<div class="row">

			<jsp:include page="../fragments/printErrorsOnPage.jsp" />
			<a href="addExam">New Exam</a>
			<div class="card mb-4">
				<div class="card-body">
					<h2 class="card-title">Exams</h2>
					<jsp:include page="../fragments/tableForView.jsp" />
					<table class="table table-hover" id="exams">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">code</th>
								<th scope="col">subject</th>
								<th scope="col">year</th>
								<th scope="col">semester</th>

								<th scope="col">professor</th>
								<th scope="col">name/surname</th>
								<th scope="col">exam date</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<nav>
						<ul class="pagination justify-content-center"
							style="flex-wrap: wrap;">
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="container fill">
			<!-- Search Widget -->
			<form:form name="exam" id="examForm" method="post"
				action="${pageContext.request.contextPath}/exam/confirm"
				modelAttribute="examDto">
				<table class="table table-hover" id="exams">

					<tbody>
						<tr>
							<spring:bind path="examdate">
								<div class="form-group ${status.error ? 'has-error' : ''}">
									<label class="col-sm-2 control-label">examdate.date (
										dd/mm/yyyy )</label>
									<div class="col-sm-10">


										<form:textarea path="examdate" rows="1" class="form-control"
											id="examdate" placeholder="examdate" required="true" />

										<form:errors path="examdate" class="control-label" />
									</div>
								</div>
							</spring:bind>
						</tr>

						<tr>
							<spring:bind path="professor">

								<form:select path="professor">
									<c:forEach items="${professors}" var="professor">
										<form:option value="${professor}">${professor}</form:option>
									</c:forEach>
								</form:select>
							</spring:bind>

						</tr>
						<tr>

						</tr>
						<tr>
							<spring:bind path="subject.id">

								<form:select path="subject.id">
									<c:forEach items="${subjects}" var="subject">
										<form:option value="${subject}">${subject}</form:option>
									</c:forEach>
								</form:select>
							</spring:bind>

						</tr>

						<tr>
							<td colspan="2" align="center"><input type="submit"
								id="save" name="save" value="save" />
					</tbody>
				</table>

			</form:form>

		</div>

	</div>
	<!-- Sidebar Widgets Column -->
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/src.js"></script>
	<script type="text/javascript"
		src="<%=request.getContextPath()%>/resources/js/exam-src.js"></script>
	<jsp:include page="../fragments/footer.jsp" />


	<script>
		$(document).ready(function() {
			loadTable(1, $('#num select').val());

			$('#num select').change(function(e) {
				loadTable(1, $('#num select').val());
			});

			$('#exams').on('click', 'tbody tr', function(e) {
				$('#modal .modal-title').html("ExamDto");
				var examDto = {
					id : this.cells[0].innerHTML

				}
				ajaxFindExam(examDto);
			});
			$('#indexnum').change(function(e) {
				ajaxFindExam($('#indexnum').val())
			});

		});
	</script>

</body>
</html>