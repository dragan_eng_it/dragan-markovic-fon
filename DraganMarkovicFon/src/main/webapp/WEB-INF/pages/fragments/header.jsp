<%@ page session="false"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel='stylesheet'
	href='<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
<link rel='stylesheet'
	href='<%=request.getContextPath()%>/resources/css/style.css'>
<%@ page isELIgnored="false"%>
<jsp:include page="menu.jsp" />
</head>