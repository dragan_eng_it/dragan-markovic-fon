<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
	<div class="container">
		<a class="navbar-brand" href="#">Fakultet</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarResponsive">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item "><a class="nav-link"
					href='<%=request.getContextPath()%>/city'>CITIES </a></li>
				<li class="nav-item "><a class="nav-link"
					href='<%=request.getContextPath()%>/student'>Students</a></li>
				<li class="nav-item"><a class="nav-link"
					href='<%=request.getContextPath()%>/professor/'> Professors</a></li>
				<li class="nav-item"><a class="nav-link"
					href='<%=request.getContextPath()%>/subject/'>Subjects</a></li>
				<li class="nav-item"><a class="nav-link"
					href='<%=request.getContextPath()%>/exam'>Exam </a></li>
				<li class="nav-item"><a class="nav-link"
					href='<%=request.getContextPath()%>/examList'>ExamList </a></li>
				<li class="nav-item"><a class="nav-link"
					href='<%=request.getContextPath()%>/role'>Role list </a></li>

			</ul>
		</div>
	</div>
</nav>