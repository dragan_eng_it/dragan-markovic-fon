	<footer class="py-1 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">2019</p>
		</div>
		<div class="modal" tabindex="-1" role="dialog" id="modal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body text-center">
						<dl>
						</dl>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</footer>

<script
	src="<%=request.getContextPath()%>/webjars/jquery/3.1.1/jquery.min.js"></script>
<script
	src="<%=request.getContextPath()%>/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>

