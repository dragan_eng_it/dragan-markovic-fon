<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<jsp:include page="../fragments/header.jsp" />

<form:form method="post"
	action="/DraganMarkovicFON/exam/prijava/confirm" modelAttribute="exam">
	<c:if test="${not empty errorMessage }">
		<div>${errorMessage}</div>
	</c:if>

	<fieldset>
		<legend>Register new exam student</legend>
		<table>
			<tr>
				<td><form:label path="examdate">examdate </form:label></td>
				<td><form:input path="examdate" /></td>
				<td><form:errors path="examdate" /></td>
			</tr>
			<tr>
				<td><form:label path="professor">professor</form:label></td>
				<td><form:input path="professor" /></td>
				<td><form:errors path="professor" /></td>
			</tr>
			<tr>
				<td><form:label path="subject">professor</form:label></td>
				<td><form:input path="subject" /></td>
				<td><form:errors path="subject" /></td>
			</tr>


			<tr>
				<td colspan="2">
					<fieldset>
						<legend>Student</legend>
						<table>
							<tr>
								<td><form:label path="studentDto">Student</form:label></td>
								<td><form:select path="studentDto">
										<form:options items="${studList}" itemValue="id"
											itemLabel="name" />
									</form:select></td>
							</tr>
						</table>
					</fieldset>

				</td>
			</tr>


			<tr>
				<td>
					<button id="save">Save</button>
				</td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</fieldset>
</form:form>