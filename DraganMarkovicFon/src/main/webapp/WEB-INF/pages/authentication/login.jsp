<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>

<body>
	
	<form:form action="/DraganMarkovicFON/authentication/login" method="post"
		modelAttribute="userDto">
		Username-:<form:input type="text" path="email" id="emailId" />
		<br />
		<form:errors path="email" cssClass="error" />
		<p />
		Password::<form:input type="text" path="password" id="passwordId" />
		<br />
		<form:errors path="password" cssClass="error" />
		<p />
		<button id="Login">Login</button>
	</form:form>
	
</body>
</html>