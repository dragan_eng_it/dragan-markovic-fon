package dragan.markovic.fon.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.model.StudentDto;

@Component(value = "studentFormValidator")
public class StudentFormValidator implements Validator {
	private CityValidator validator=new CityValidator();

	public StudentFormValidator() {
		if (validator == null) {
			throw new IllegalArgumentException("The supplied [cityValidator] is " + "required and must not be null.");
		}
		if (!validator.supports(CityDto.class)) {
			throw new IllegalArgumentException(
					"The supplied [cityValidator] must " + "support the validation of [cityentity] instances.");
		}
	}

	/**
	 * This Validator validates StudentDto for addStudent form instances, and any subclasses of
	 * entity too
	 */
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indexnumber", "field.required");

		StudentDto s = (StudentDto) target;
		if (s.getId() <= 0) {
			errors.rejectValue("id", "negativeValue", new Object[] { "'id'" }, "id can't be negative");
		}

		try {
			errors.pushNestedPath("city");
			ValidationUtils.invokeValidator(this.validator, s.getCity(), errors);
		} finally {
			errors.popNestedPath();
		}
	}
}
