package dragan.markovic.fon.validator;

import static dragan.markovic.fon.util.St.valueOf;

import java.time.LocalDate;
import java.util.Calendar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import dragan.markovic.fon.validator.Year;

public class YearConstraintValidator implements ConstraintValidator<Year, LocalDate> {

	private int annotationYear;
	
	@Override
	public void initialize(Year year) {
		this.annotationYear = year.value();
	}

	@Override
	public boolean isValid(LocalDate target, ConstraintValidatorContext cxt) {
		if(target == null) {
			return valueOf(true);
		}
		Calendar c = Calendar.getInstance();
		int fieldYear = c.get(Calendar.YEAR);
		return valueOf(fieldYear == annotationYear);
	}

}
