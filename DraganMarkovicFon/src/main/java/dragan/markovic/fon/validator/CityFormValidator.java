package dragan.markovic.fon.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dragan.markovic.fon.model.CityDto;

@Component(value = "cityFormValidator")
public class CityFormValidator implements Validator {

	//which objects can be validated by this validator
	@Override
	public boolean supports(Class<?> paramClass) {
		return CityDto.class.equals(paramClass);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		
		System.out.println("validator form city works");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "number.required");
		CityDto p = (CityDto) obj;
		if (p.getName().length() < 3) {
			errors.rejectValue("name", "name.lenght3");
		}
		if (p.getName().length() > 30) {
			errors.rejectValue("name", "name.lenght30");
		}
		if (p.getNumber().length() > 30) {
			errors.rejectValue("number", "number.lenght30");
		}
	}
}