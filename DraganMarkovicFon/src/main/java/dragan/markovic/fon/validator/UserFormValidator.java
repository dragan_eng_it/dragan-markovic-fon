package dragan.markovic.fon.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dragan.markovic.fon.model.UserDto;
import dragan.markovic.fon.service.UserService;

public class UserFormValidator implements Validator {

	@Autowired
	@Qualifier("emailValidator")
	EmailValidator emailValidator;
	
	public UserFormValidator() {
		super();
	}

	@Autowired
	UserService userService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return UserDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UserDto user = (UserDto) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.userForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.userForm.email");

		if(!emailValidator.valid(user.getEmail())){
			errors.rejectValue("email", "Pattern.userForm.email");
		}
		
		
		if (user.getPassword().length()<3) {
			errors.rejectValue("length", "short length");
		}
		
		

	}

}