package dragan.markovic.fon.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static dragan.markovic.fon.util.St.valueOf;;

public class PhoneValidator implements ConstraintValidator<Phone, String> {

	@Override
	public void initialize(Phone paramA) {
	}

	@Override
	public boolean isValid(String phoneNo, ConstraintValidatorContext ctx) {
		// empty is ok
		
		if(phoneNo == null){
			return valueOf(true);
		}
		//validate phone numbers of format "1234567890"
        if (phoneNo.matches("\\d{10}")) return valueOf(true);
        //validating phone number with -, . or spaces
        else if(phoneNo.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return valueOf(true);
        //validating phone number with extension length from 3 to 5
        else if(phoneNo.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return valueOf(true);
        //validating phone number where area code is in braces ()
        else if(phoneNo.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return valueOf(true);
        //return false if nothing matches the input
        else return valueOf(false);
	}

}
