package dragan.markovic.fon.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dragan.markovic.fon.model.CityDto;
@Component(value = "cityValidator")
public class CityValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return CityDto.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmpty(e, "name", "name.required");
		ValidationUtils.rejectIfEmpty(e, "number", "number.required");
		CityDto p = (CityDto) obj;
		if (p.getName().length() < 3) {
			e.rejectValue("name", "name.lenght3");
		}
		if (p.getName().length() > 30) {
			e.rejectValue("name", "name.lenght30");
		}
		if (p.getNumber().length() > 30) {
			e.rejectValue("number", "number.lenght30");
		}

	}

}
