package dragan.markovic.fon.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import dragan.markovic.fon.entity.ProfessorEntity;
import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.model.ProfessorDto;

@Component(value = "professorValidator")
public class ProfessorValidator implements Validator {
	private CityValidator validator=new CityValidator();

	public ProfessorValidator() {
		if (validator == null) {
			throw new IllegalArgumentException("The supplied [cityValidator] is " + "required and must not be null.");
		}
		if (!validator.supports(CityDto.class)) {
			throw new IllegalArgumentException(
					"The supplied [cityValidator] must " + "support the validation of [cityentity] instances.");
		}
	}

	/**
	 * This Validator validates ProfessorEntity instances, and any subclasses of
	 * ProfessorEntity too
	 */
	public boolean supports(Class<?> clazz) {
		return ProfessorDto.class.isAssignableFrom(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "field.required");
		ProfessorEntity professor = (ProfessorEntity) target;
		if (professor.getId() <= 0) {
			errors.rejectValue("id", "negativeValue", new Object[] { "'id'" }, "id can't be negative");
		}

		try {
			errors.pushNestedPath("city");
			ValidationUtils.invokeValidator(this.validator, professor.getCity(), errors);
		} finally {
			errors.popNestedPath();
		}
	}
}
