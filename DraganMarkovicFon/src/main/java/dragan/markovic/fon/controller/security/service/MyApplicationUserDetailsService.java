package dragan.markovic.fon.controller.security.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface MyApplicationUserDetailsService extends UserDetailsService{

}
