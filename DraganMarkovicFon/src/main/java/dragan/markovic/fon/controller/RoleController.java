package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dragan.markovic.fon.model.RoleDto;
import dragan.markovic.fon.service.RoleService;


@Controller
@RequestMapping(value = "/role")
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	
	@ModelAttribute("roleDto")
	public RoleDto createRoleDtoModel() {
		// ModelAttribute value should be same as used in the addx.jsp
		return new RoleDto();
	}
	
	@GetMapping
	public String home() {
		return "role/home";
	}
	
	
	@GetMapping(value = "/add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("role/addRole");
		
		RoleDto roleDto = new RoleDto();
		modelAndView.addObject("roleDto",roleDto);
		
		return modelAndView;
	}
	
	@PostMapping
	public ModelAndView save(@ModelAttribute("roleDto") @Validated RoleDto roleDto, BindingResult result) throws SQLIntegrityConstraintViolationException {
		
		if (result.hasErrors()) {
			ModelAndView modelAndView=new ModelAndView("role/addRole");
			modelAndView.addObject("roleDto",roleDto);
			return modelAndView;
		}else {
			ModelAndView modelAndView=new ModelAndView("role/home");
		    roleDto = roleService.insert(roleDto);
			return modelAndView;
		}
	}
	
}
