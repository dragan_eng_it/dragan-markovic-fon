package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import dragan.markovic.fon.enums.Semester;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.model.SubjectDto;
import dragan.markovic.fon.service.SubjectService;

@Controller
@RequestMapping(value = "subject")
@SessionAttributes(value = "subjectForm")
public class SubjectController {

	@Autowired
	private SubjectService subjectService;

	@ModelAttribute(value = "semesters")
	public List<Semester> getSemesters() {
		
		return Arrays.asList(Semester.class.getEnumConstants());
	}

	@ModelAttribute(value = "subjectForm")
	public SubjectDto createSubjectDtoModel() {
		return new SubjectDto();
	}

	@ModelAttribute(value = "numberList")
	public List<Integer> populateNumberList() {

		return  Arrays.asList(1, 2, 3,4);
	}

	@GetMapping
	public String home(Model modelAndView) {
		List<SubjectDto> listOfSubjectsDto = subjectService.getAll();
		// modelAndView.setViewName("subject/viewSubjects");
		modelAndView.addAttribute("listOfSubjectsDto", listOfSubjectsDto);
		return "subject/viewSubjects";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "redirect:/subject";
	}

	@GetMapping(value = "/add")
	public String add(Model modelAndView) {

		modelAndView.addAttribute("subjectForm", new SubjectDto());
		modelAndView.addAttribute("semesters", getSemesters());
		modelAndView.addAttribute("numberList", populateNumberList());
		return "subject/addSubject";
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@Valid @ModelAttribute(name = "subjectForm") SubjectDto subjectDto, Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {

			modelAndView.setViewName("subject/addSubject");
		} else {
			modelAndView.setViewName("subject/add_confirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "/save")
	public String save(@ModelAttribute(name = "subjectForm") SubjectDto subjectDto,
			@RequestParam(name = "action") String action, SessionStatus sessionStatus)
			throws SQLIntegrityConstraintViolationException {

		if (action.equalsIgnoreCase("save")) {
			System.out.println("stiglo iz confirma " + subjectDto.toString());

			subjectService.insert(subjectDto);
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("cancel")) {
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("change")) {

		}

		return "subject/viewSubjects";
	}

	@GetMapping("/update/{id}")
	public String showFormForUpdate(@PathVariable("id") final int id, Model model) throws RecordNotFoundException {
		SubjectDto subjectDto = subjectService.findById((long) id);
		model.addAttribute("subjectForm", subjectDto);
		model.addAttribute("semesters", getSemesters());
		model.addAttribute("numberList", populateNumberList());

		return "subject/addSubject";
	}

	// delete user
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") final int id, final RedirectAttributes redirectAttributes) {

		subjectService.deleteById((long) id);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "User is deleted!");
		return "redirect:/subject";

	}

	// show entity
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") final int id, Model model) {

		SubjectDto s = subjectService.findById((long) id);
		if (s == null) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "This not found");
		} else {
		}
		model.addAttribute("subjectForm", s);

		return "subject/show";

	}

	
}
