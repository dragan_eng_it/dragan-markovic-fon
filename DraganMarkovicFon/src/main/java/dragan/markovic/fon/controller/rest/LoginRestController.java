package dragan.markovic.fon.controller.rest;


import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.UserDto;

@RestController
@RequestMapping(path = "api/login")
public class LoginRestController {

	@PostMapping
	public ResponseEntity<UserDto> login(@RequestBody UserDto userDto) {
		System.out.println(userDto);
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	
	}
}
