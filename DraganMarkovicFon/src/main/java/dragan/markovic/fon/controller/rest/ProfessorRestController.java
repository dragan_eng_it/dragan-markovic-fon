package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.ProfessorDto;
import dragan.markovic.fon.service.ProfessorService;
import static dragan.markovic.fon.util.St.DATE_FMT;
import static dragan.markovic.fon.util.St.valueOf;

@RestController
@RequestMapping(path = "/api/professor")
public class ProfessorRestController {
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        DATE_FMT.setLenient(valueOf(true));
        binder.registerCustomEditor(Date.class,"reelectiondate", new CustomDateEditor(DATE_FMT, valueOf(false)));
                     
    }
	@Autowired
	private ProfessorService professorService;

	@GetMapping
	public List<ProfessorDto> getAll(HttpServletRequest request) {
		List<ProfessorDto> result = professorService.getAll();
		return result;
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProfessorDto> getById(
			@PathVariable @Min(value = 1, message = "id must be greater than or equal to 1") @Max(value = 1000, message = "id must be lower than or equal to 1000") final Long id) {
		return new ResponseEntity<>(professorService.findById(id), HttpStatus.OK);
	}
	
	
	@PutMapping("{id}")
	public ProfessorDto update(@RequestBody ProfessorDto professorDto, @PathVariable final Long id) throws SQLIntegrityConstraintViolationException {

		return professorService.update(professorDto, id);
	}
	
	
	@PostMapping
	public ResponseEntity<ProfessorDto> insert(@RequestBody ProfessorDto professorDto, HttpServletRequest request) throws SQLIntegrityConstraintViolationException {
		return new ResponseEntity<>(professorService.insert(professorDto), HttpStatus.OK);
	}

	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<ProfessorDto> findBy(@RequestBody ProfessorDto professorDto, HttpServletRequest request) {

		return new ResponseEntity<>(professorService.findById((long)professorDto.getId()), HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<ProfessorDto>> pagination(@RequestParam final int page, @RequestParam final int num,
			HttpServletRequest request) {
		return new ResponseEntity<List<ProfessorDto>>(professorService.getPage(page, num), HttpStatus.OK);
	}

	@GetMapping
	@RequestMapping(path = "totalCount")
	public int totalCount(HttpServletRequest request) {

		return professorService.getTotalCount();
	}
	
}

