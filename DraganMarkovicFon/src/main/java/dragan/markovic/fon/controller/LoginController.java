package dragan.markovic.fon.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dragan.markovic.fon.model.UserDto;

@Controller
@RequestMapping(path = "/login")
public class LoginController {

	@GetMapping
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("index");
		modelAndView.addObject("userDto",new UserDto());
		return modelAndView;
	}
	
	
	@PostMapping
	public ResponseEntity<UserDto> login(@RequestBody UserDto userDto) {
		System.out.println(userDto);
		return new ResponseEntity<>(userDto, HttpStatus.OK);
	
	}
}
