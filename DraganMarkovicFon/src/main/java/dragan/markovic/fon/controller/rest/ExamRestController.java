package dragan.markovic.fon.controller.rest;

import java.sql.Date;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.ExamDto;
import dragan.markovic.fon.service.ExamService;
import static dragan.markovic.fon.util.St.DATE_FMT;
import static dragan.markovic.fon.util.St.valueOf;

@RestController
@RequestMapping(path = "/api/exam")
public class ExamRestController {
	@Autowired
	private ExamService examService;

	@GetMapping
	public List<ExamDto> getAll() {
		return examService.getAll();
	}
	@InitBinder
	public void bindingPreparation(WebDataBinder binder) {
		
     DATE_FMT.setLenient(valueOf(false));
	  CustomDateEditor orderDateEditor = new CustomDateEditor(DATE_FMT, true);
	  binder.registerCustomEditor(Date.class, orderDateEditor);
	}
	@GetMapping("/{id}")
	public ResponseEntity<ExamDto> getById(@PathVariable Long id) {
		
		return new ResponseEntity<>(examService.findById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ExamDto> insert(@RequestBody ExamDto examDto,
			HttpServletRequest request) throws SQLIntegrityConstraintViolationException {
		examDto = examService.insert(examDto);
		return new ResponseEntity<>(examDto, HttpStatus.OK);
	}
	
	@PutMapping("{id}")
	public ExamDto Update(@RequestBody ExamDto examDto, @PathVariable Long id) throws SQLIntegrityConstraintViolationException {

		return examService.update(examDto, id);
	}
	
	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<ExamDto> findBy(@RequestBody ExamDto examDto,
			HttpServletRequest request) {
		ExamDto exam = examService.findById(examDto.getId());

		return new ResponseEntity<>(exam, HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<ExamDto>> pagination(@RequestParam int page, @RequestParam int num,
			HttpServletRequest request) {
		System.out.println("ExamRestController:: pagination");
		List<ExamDto> listOfSub = examService.getPage(page, num);
		return new ResponseEntity<List<ExamDto>>(listOfSub, HttpStatus.OK);
	}

	@GetMapping
	@RequestMapping(path = "totalCount")
	public int totalCount(HttpServletRequest request) {

		return examService.getTotalCount();
	}

}
