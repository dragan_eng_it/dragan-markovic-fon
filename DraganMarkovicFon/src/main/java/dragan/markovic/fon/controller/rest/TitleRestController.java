package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.TitleDto;
import dragan.markovic.fon.service.TitleService;

@RestController
@RequestMapping(path = "api/title")
public class TitleRestController {
	@Autowired
	private TitleService titleService;
	
	@GetMapping
	public List<TitleDto> getAll( ) {
		return titleService.getAll();
	}

	@PostMapping
	public TitleDto create(@RequestBody TitleDto titleDto) throws SQLIntegrityConstraintViolationException {
		return titleService.insert(titleDto);
	}

	@GetMapping("{id}")
	public TitleDto getById(
			@PathVariable @Min(value = 1, message = "id must be greater than or equal to 1") @Max(value = 1000, message = "id must be lower than or equal to 1000") final Long id) {
		return titleService.findById(id);
	}
	
	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<TitleDto> findBy(@RequestBody TitleDto titleDto, HttpServletRequest request) {
		return new ResponseEntity<>(titleService.findById(titleDto.getId()), HttpStatus.OK);
	}

	@PutMapping("{id}")
	public TitleDto Update(@RequestBody TitleDto titleDto, @PathVariable final Long id) throws SQLIntegrityConstraintViolationException {

		return titleService.update(titleDto, id);

	}

	@DeleteMapping("{id}")
	void delete(@PathVariable Long id) {
		titleService.deleteById(id);
	}
	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<TitleDto>> pagination(@RequestParam final int page, @RequestParam final int num,
			HttpServletRequest request) {
		return new ResponseEntity<List<TitleDto>>(titleService.getPage(page, num), HttpStatus.OK);
	}
	@GetMapping
	@RequestMapping(path = "totalCount")
	public ResponseEntity<Integer> totalCount(HttpServletRequest request) {

		return new ResponseEntity<Integer>(titleService.getTotalCount(), HttpStatus.OK);
	}
}
