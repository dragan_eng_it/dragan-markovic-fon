package dragan.markovic.fon.controller.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import dragan.markovic.fon.controller.security.MyApplicationInMemoryPrincipal;
import dragan.markovic.fon.controller.security.service.MyApplicationUserDetailsService;


public class MyApplicationInMemoryUserDetailsService  implements MyApplicationUserDetailsService{
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (username.equals("admin")) 
			return new MyApplicationInMemoryPrincipal(username, passwordEncoder.encode("1234"));
		throw new UsernameNotFoundException("User does not exist!");
	}

}
