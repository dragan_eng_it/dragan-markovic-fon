package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.RoleDto;
import dragan.markovic.fon.service.RoleService;

@RestController
@RequestMapping(path = "/api/role", produces = { MediaType.APPLICATION_JSON_VALUE })
public class RoleRestController {

	private final RoleService roleService;

	@Autowired
	public RoleRestController(RoleService roleService) {
		this.roleService = roleService;
	}

	@GetMapping
	public List<RoleDto> getAll() {
		return roleService.getAll();
	}

	@PostMapping
	RoleDto createOrSave(@RequestBody RoleDto roleDto) throws SQLIntegrityConstraintViolationException {
		return roleService.insert(roleDto);
	}

	@GetMapping("/{id}")
	RoleDto getEmployeeById(
			@PathVariable @Min(value = 1, message = "id must be greater than or equal to 1") @Max(value = 1000, message = "id must be lower than or equal to 1000") final Long id) {
		return roleService.findById(id);
				}

	@PutMapping("/{id}")
	public RoleDto saveOrUpdate(@RequestBody RoleDto roleDto, @PathVariable final Long id) throws SQLIntegrityConstraintViolationException {

		roleService.update(roleDto,id);
		
		return roleDto;
	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable final Long id) {
		roleService.deleteById(id);
	}
}
