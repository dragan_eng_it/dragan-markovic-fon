package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.model.ProfessorDto;
import dragan.markovic.fon.model.TitleDto;
import dragan.markovic.fon.service.CityService;
import dragan.markovic.fon.service.ProfessorService;
import dragan.markovic.fon.service.TitleService;
import dragan.markovic.fon.util.St;

@Controller
@RequestMapping(value = "/professor")
@SessionAttributes(value = "professorDto")
public class ProfessorController {


	@InitBinder
	public void initBinder(WebDataBinder binder) {
		St.DATE_FMT.setLenient(St.valueOf(true));
		binder.registerCustomEditor(Date.class, "reelectiondate", new CustomDateEditor(St.DATE_FMT, St.valueOf(false)));
		// binder.registerCustomEditor(CityDto.class,"city", new CityEditor());

	}

	@Autowired
	@Qualifier("studentFormValidator")
	Validator validator;

	@Autowired
	private ProfessorService professorService;
	@Autowired
	CityService cityService;

	@Autowired
	TitleService titleService;

	@ModelAttribute("professorDto")
	public ProfessorDto getProfessorDto() {
		return new ProfessorDto();
	}

	// @ModelAttribute("cities")
	public List<CityDto> getAllCities() {
		return cityService.getAll();
	}

	// @ModelAttribute("titles")
	public List<TitleDto> getAllTitles() {
		return titleService.getAll();
	}

	@GetMapping
	public String home(Model modelAndView) {

		return "professor/viewProfessors";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "redirect:/professor";
	}

	@GetMapping(value = "/add")
	public String add(Model modelAndView) {

		modelAndView.addAttribute("professorDto", new ProfessorDto());
		modelAndView.addAttribute("cities", cityService.getAll());
		modelAndView.addAttribute("titles", titleService.getAll());
		return "professor/addProfessor";
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@Valid @ModelAttribute("professorDto") ProfessorDto  professorDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("cities", cityService.getAll());
		modelAndView.addObject("titles", titleService.getAll());

		if (errors.hasErrors()) {
			modelAndView.addObject("css", "danger");

			modelAndView.addObject("msg", "There is something wrong..");
			System.out.println("ima gresaka pa ne ie na confitm" + professorDto.toString());
			modelAndView.setViewName("professor/addProfessor");
		} else {
			modelAndView.setViewName("professor/add_confirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "save")
	public String save(@ModelAttribute("professorDto") ProfessorDto professorDto,
			@RequestParam("action") final String action, SessionStatus sessionStatus)
			throws SQLIntegrityConstraintViolationException {
		if (action.equalsIgnoreCase("save")) {
			professorService.insert(professorDto);
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("cancel")) {
		}
		if (action.equalsIgnoreCase("change")) {
			return ("professor/addProfessor");
		}
		if (action.equalsIgnoreCase("update")) {
			professorService.update(professorDto, professorDto.getId());
			sessionStatus.setComplete();
		}
		return "redirect:/viewProfessors";
	}

	@GetMapping("/update/{id}")
	public String showFormForUpdate(@PathVariable("id") final int id, Model model) throws RecordNotFoundException {

		model.addAttribute("professorDto", professorService.findById((long) id));
		model.addAttribute("cities", cityService.getAll());
		model.addAttribute("titles", titleService.getAll());

		return "professor/addProfessor";
	}

	// delete user
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") final int id, final RedirectAttributes redirectAttributes) {

		professorService.deleteById((long) id);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "User is deleted!");
		return "redirect:/professor";

	}

	// show entity
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") final int id, Model model) {

		ProfessorDto s = professorService.findById((long) id);
		if (s == null) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "This not found");
		} else {
			s.setCity(cityService.findById((long) s.getCity().getId()));
			s.setTitle(titleService.findById((long) s.getTitle().getId()));
		}
		model.addAttribute("professorForm", s);

		return "professor/show";

	}
}
