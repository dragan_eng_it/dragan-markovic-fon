package dragan.markovic.fon.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@GetMapping
	public String home() {
		return "index";
	}

	@GetMapping
	@RequestMapping(path = "viewCities")
	public String viewCity() {
		return "forward:city";
	}

	@GetMapping
	@RequestMapping(path = "viewStudents")
	public String viewStudents() {
		return "forward:student";
	}

	@GetMapping
	@RequestMapping(path = "viewProfessors")
	public String viewProfessor() {
		return "forward:professor";
	}

	@GetMapping
	@RequestMapping(path = "viewSubjects")
	public String viewSubject() {
		return "forward:subject";
	}

	@GetMapping
	@RequestMapping(path = "viewExams")
	public String viewExam() {
		return "forward:exam";
	}

	@GetMapping
	@RequestMapping(path = "viewExamsList")
	public String viewExamList() {
		return "forward:examList";
	}
	@GetMapping
	@RequestMapping(path = "viewRole")
	public String viewRole() {
		return "forward:role";
	}

	@GetMapping
	@RequestMapping(path = "addCity")
	public String addCity() {
		return "forward:city/add";
	}
	@GetMapping
	@RequestMapping(path = "addProfessor")
	public String addProfessor() {
		return "forward:professor/add";
	}

	@GetMapping
	@RequestMapping(path = "addSubject")
	public String addSubject() {
		return "forward:subject/add";
	}

	@GetMapping
	@RequestMapping(path = "addStudent")
	public String addStudent() {
		return "forward:student/add";
	}

	@GetMapping
	@RequestMapping(path = "addExam")
	public String addExam() {
		return "forward:exam/add";
	}

	@GetMapping
	@RequestMapping(path = "addTitle")
	public String addTitle() {
		return "forward:title/add";
	}
	@GetMapping
	@RequestMapping(path = "addRole")
	public String addRole() {
		return "forward:role/add";
	}

	@GetMapping
	@RequestMapping(path = "/error/globalException")
	public String errorGlobalException() {
		return "forward:/error/globalException";
	}
}
