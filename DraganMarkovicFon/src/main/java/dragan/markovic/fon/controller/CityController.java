package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

//import javax.validation.ConstraintViolationException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import dragan.markovic.fon.exception.MyValidationException;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.service.CityService;

@Controller
@RequestMapping(value = "/city")
//@SessionAttributes(value = "cityForm")
public class CityController {

	@Autowired
	CityService cityService;

	@Autowired
	@Qualifier("cityFormValidator")
	Validator validator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(value = "cityForm")
	public CityDto createCityDtoModel() {
		return new CityDto();
	}

	// @ModelAttribute(value = "cities")
	public List<CityDto> getAllCities(Model model) {
		return cityService.getAll();
	}

	@GetMapping
	public String home(Model modelAndView) {

		return "city/viewCities";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(ModelAndView modelAndView) {
		modelAndView.setViewName("city/viewCity");
		return modelAndView;
	}

	// show entity
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") final int id, Model model) {

		CityDto s = cityService.findById((long) id);
		if (s == null) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "This not found");
		} else {
		}
		model.addAttribute("cityForm", s);

		return "city/show";

	}

	@GetMapping(value = "/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("city/addCity");
		return modelAndView;
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@Valid @ModelAttribute("cityForm") CityDto cityDto, BindingResult bindingResult) {
		// validator.validate(cityDto, bindingResult);
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("city/addCity");
			modelAndView.addObject("errorMessage", "somwthing is wrong " + bindingResult.getErrorCount());
			return modelAndView;

		}
		ModelAndView modelAndView = new ModelAndView("city/add_confirm");

		return modelAndView;
	}

	@PostMapping("save")
	public ModelAndView save(@Valid @ModelAttribute("cityForm") CityDto cityDto,
			@RequestParam(name = "action") String action, BindingResult bindingResult, SessionStatus sessionStatus)
			throws SQLIntegrityConstraintViolationException {

		ModelAndView modelAndView = new ModelAndView("city/addCity");
		if (bindingResult.hasErrors()) {
			modelAndView.addObject("errorMessage", "somwthing is wrong " + bindingResult.getErrorCount());
		} else {

			if (action.equalsIgnoreCase("save")) {
				cityDto = cityService.insert(cityDto);
				modelAndView.addObject("message", "city is saved " + cityDto.toString());
				sessionStatus.setComplete();

			}
			if (action.equalsIgnoreCase("cancel")) {
				sessionStatus.setComplete();
			}
			if (action.equalsIgnoreCase("change")) {

			}
		}
		return modelAndView;
	}

	@GetMapping("/update/{id}")
	public String showFormForUpdate(@PathVariable("id") final int id, Model model) throws RecordNotFoundException {
		CityDto cityDto = cityService.findById((long) id);
		model.addAttribute("cityForm", cityDto);
		return "city/addCity";
	}

	// delete user
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") final int id, final RedirectAttributes redirectAttributes) {

		cityService.deleteById((long) id);
		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "City id : " + id + " is deleted!");
		return "redirect:/city";

	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ModelAndView exceptionHandler(MyValidationException validationException, HttpServletRequest request) {
		System.out.println("@ExceptionHandler exception ocured: MyValidationException===========");
		ModelAndView modelAndView = new ModelAndView("city/addCity");
		modelAndView.addObject("errorMessage", validationException.getMessage());
		modelAndView.addObject("cityForm", request.getAttribute("cityForm"));
		return modelAndView;
	}
}
