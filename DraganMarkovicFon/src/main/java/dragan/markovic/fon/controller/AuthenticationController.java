package dragan.markovic.fon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dragan.markovic.fon.config.security.MyApplicationInMemoryAuthenticationProvider;
import dragan.markovic.fon.controller.security.MyApplicationInMemoryPrincipal;
import dragan.markovic.fon.model.UserDto;


//@Controller
@RequestMapping(value = "authentication")
public class AuthenticationController {
	@Autowired
	private MyApplicationInMemoryAuthenticationProvider myApplicationInMemoryAuthenticationProvider;
	
	@GetMapping(value = "login")
	public ModelAndView login() {
		return new ModelAndView("authentication/login");
	}
	
	@PostMapping(value = "login")
	public ModelAndView authenticate(@ModelAttribute ("userDto") UserDto userDto) {
		System.out.println("AuthenticationController: authenticate()=======================");
		System.out.println(userDto);
		
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.setViewName("subject/addSubject");
		
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDto.getEmail(),userDto.getPassword());
		Authentication authentication =  myApplicationInMemoryAuthenticationProvider.authenticate(token);
		
		if (authentication==null) {
			modelAndView.setViewName("authentication/login");
		}else {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			
			System.out.println("Login as: "+(MyApplicationInMemoryPrincipal)authentication.getPrincipal());
		}
		
		
		return modelAndView;
	}
	
	@ModelAttribute(name = "userDto")
	private UserDto generateUserDto() {
		return new UserDto();
	}
}
