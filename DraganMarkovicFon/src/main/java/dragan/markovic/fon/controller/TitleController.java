package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dragan.markovic.fon.model.TitleDto;
import dragan.markovic.fon.service.TitleService;


@Controller
@RequestMapping(value = "/title")
public class TitleController {
	
	@Autowired
	private TitleService titleService;
	
	@ModelAttribute("titleDto")
	public TitleDto createTitleDtoModel() {
		return new TitleDto();
	}
	
	@GetMapping
	public String home() {
		return "title/home";
	}
	
	
	@GetMapping(value = "/add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("title/addTitle");
		
		TitleDto titleDto = new TitleDto();
		modelAndView.addObject("titleDto",titleDto);
		
		return modelAndView;
	}
	
	@PostMapping
	public ModelAndView save(@ModelAttribute("titleDto") @Validated TitleDto titleDto, BindingResult result) throws SQLIntegrityConstraintViolationException {
		
		if (result.hasErrors()) {
			ModelAndView modelAndView=new ModelAndView("title/add");
			modelAndView.addObject("titleDto",titleDto);
			return modelAndView;
		}else {
			ModelAndView modelAndView=new ModelAndView("title/home");
		    titleDto = titleService.insert(titleDto);
			return modelAndView;
		}
	}
	
}
