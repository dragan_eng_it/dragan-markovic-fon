package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.StudentExamDto;
import dragan.markovic.fon.service.StudentExamService;

@RestController
@RequestMapping(path = "/api/studentExam")
public class StudentExamRestController {
	@Autowired
	private StudentExamService studentExamService;

	@GetMapping
	public List<StudentExamDto> getAll(HttpServletRequest request) {
		System.out.println("StudentExamRestController:: getall()");
		List<StudentExamDto> result = studentExamService.getAll();
		return result;
	}

	@GetMapping("/{id}")
	public ResponseEntity<StudentExamDto> getById(@PathVariable Long id) {
		return new ResponseEntity<>(studentExamService.findById(id), HttpStatus.OK);
	}

	// studentExamDto,titleDto,cityDto
	@PostMapping
	public ResponseEntity<StudentExamDto> insert(@RequestBody StudentExamDto studentExamDto,
			HttpServletRequest request) throws SQLIntegrityConstraintViolationException {
		System.out.println("StudentExamRestController:: save()" + studentExamDto.toString());
		studentExamDto = studentExamService.insert(studentExamDto);
		return new ResponseEntity<>(studentExamDto, HttpStatus.OK);
	}

	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<StudentExamDto> findBy(@RequestBody StudentExamDto studentExamDto,
			HttpServletRequest request) {
		StudentExamDto studentExam = studentExamService.findById(studentExamDto.getId());

		return new ResponseEntity<>(studentExam, HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<StudentExamDto>> pagination(@RequestParam int page, @RequestParam int num,
			HttpServletRequest request) {
		System.out.println("StudentExamRestController:: pagination");
		List<StudentExamDto> listOfSub = studentExamService.getPage(page, num);
		return new ResponseEntity<List<StudentExamDto>>(listOfSub, HttpStatus.OK);
	}

	@GetMapping
	@RequestMapping(path = "totalCount")
	public int totalCount(HttpServletRequest request) {

		return studentExamService.getTotalCount();
	}

}
