package dragan.markovic.fon.controller;

import java.sql.Date;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.model.ExamDto;
import dragan.markovic.fon.model.ProfessorDtoForExam;
import dragan.markovic.fon.model.StudentDto;
import dragan.markovic.fon.model.SubjectDto;
import dragan.markovic.fon.service.ExamService;
import dragan.markovic.fon.service.ProfessorService;
import dragan.markovic.fon.service.StudentService;
import dragan.markovic.fon.service.SubjectService;
import dragan.markovic.fon.util.ProfessorDtoForExamEditor;
import dragan.markovic.fon.util.St;

@Controller
@RequestMapping(value = "/exam")
@SessionAttributes(value = "examDto")
public class ExamController {

	@Autowired
	private ExamService examService;

	@Autowired
	private StudentService studentService;
	@Autowired
	private ProfessorService professorService;

	@Autowired
	private SubjectService subjectService;

	@ModelAttribute("examDto")
	public ExamDto getExamDto() {
		return new ExamDto();
	}

	@InitBinder
	public void customizeBinding(WebDataBinder binder) {
St.DATE_FMT.setLenient(St.valueOf(false));
		binder.registerCustomEditor(Date.class, "examdate", new CustomDateEditor(St.DATE_FMT, St.valueOf(false)));
		 binder.registerCustomEditor(ProfessorDtoForExam.class, new ProfessorDtoForExamEditor());
	}

	
	@ModelAttribute("professors")
	public List<ProfessorDtoForExam> getAllProfessors() {
		List<ProfessorDtoForExam> lista = professorService.getAllforExam();
		
		return lista;
	}

	@ModelAttribute("subjects")
	public List<SubjectDto> getAllSubjects() {
		return subjectService.getAll();
	}

	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("exam/viewExams");
		ExamDto examDto = new ExamDto();

		// mav.addObject("listOfExamsDto", listOfExamsDto);
		modelAndView.addObject("examDto", examDto);
		modelAndView.addObject("professors", getAllProfessors());
		modelAndView.addObject("subjects", getAllSubjects());
		return modelAndView;
	}

	@GetMapping(value = "/add")
	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("exam/addExam");

		ExamDto examDto = new ExamDto();
		modelAndView.addObject("examDto", examDto);
		modelAndView.addObject("professors", getAllProfessors());
		modelAndView.addObject("subjects", getAllSubjects());
		return modelAndView;
	}

	@PostMapping(value = "bigdata")
	public ModelAndView save(@ModelAttribute(name = "examDto") ExamDto examDto,
			@RequestParam(name = "action") String action, @RequestParam(name = "professorDto") String professorDto,
			@RequestParam(name = "subjectDto") String titleDto, SessionStatus sessionStatus, BindingResult result)
			throws SQLIntegrityConstraintViolationException {

		if (result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("exam/add");
			modelAndView.addObject("examDto", examDto);
			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView("exam/home");
			System.out.println(examDto);
			examDto = examService.insert(examDto);
			return modelAndView;
		}
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@ModelAttribute("examDto") ExamDto examDto, Errors errors) {
		System.out.println("stigo u exam controller");
		System.out.println(examDto.toString());
System.out.println(examDto.getProfessor().getClass().getName());
		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			System.out.println("greske "+examDto.toString());
			modelAndView.setViewName("exam/viewExams");
		} else {
			System.out.println(examDto.toString());

			modelAndView.setViewName("exam/add_confirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "save")
	public String save(@ModelAttribute("examDto") ExamDto examDto, @RequestParam(name = "action") String action,
			SessionStatus sessionStatus) throws SQLIntegrityConstraintViolationException {

		if (action.equalsIgnoreCase("save")) {
			System.out.println("stiglo iz confirma " + examDto.toString());

			examService.insert(examDto);
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("cancel")) {
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("change")) {

		}

		return "exam/addExam";
	}

	@GetMapping("/update/{id}")
	public String showFormForUpdate(@PathVariable("id") int id, Model model) throws RecordNotFoundException {
		ExamDto examDto = examService.findById((long) id);
		model.addAttribute("examDto", examDto);
		model.addAttribute("professors", getAllProfessors());
		model.addAttribute("subjects", getAllSubjects());

		return "exam/addExam";
	}

	// delete user
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {

		examService.deleteById((long) id);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "User is deleted!");
		return "redirect:/exam";

	}

	// show entity
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") int id, Model model) {

		ExamDto s = examService.findById((long) id);
		if (s == null) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "This not found");
		} else {
		}
		model.addAttribute("examDto", s);

		return "exam/show";

	}

	@PostMapping(value = "prijava")
	public ModelAndView prijava(@RequestBody ExamDto examDto, ModelAndView model, Errors errors) {
		ExamDto exm = examService.findById(examDto.getId());
		if (errors.hasErrors()) {
			model.addObject("exam", exm);
			List<StudentDto> studList = studentService.getAll();
			model.addObject("studList", studList);
			model.setViewName("exam/addExamStud");

		} else {
			model.setViewName("exam/add_confirm");
		}

		return model;
	}
//	@ExceptionHandler(MyValidationException.class)
//	public ModelAndView exceptionHandler(MyValidationException validationException) {
//		System.out.println("@ExceptionHandler exception ocured: MyValidationException===========");
//		ModelAndView modelAndView = new ModelAndView("exam/add");
//		modelAndView.addObject("errorMessage", validationException.getMessage());
//		modelAndView.addObject("examForm", getExamDto());
//		return modelAndView;
//	}

}
