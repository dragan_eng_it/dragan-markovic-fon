package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.SubjectDto;
import dragan.markovic.fon.service.SubjectService;

@RestController
@RequestMapping(path = "/api/subject")
public class SubjectRestController {
	@Autowired
	private SubjectService subjectService;

	@GetMapping
	public ResponseEntity<List<SubjectDto>> getAll(HttpServletRequest request) {
		return new ResponseEntity<List<SubjectDto>>(subjectService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<SubjectDto> getById(
			@PathVariable @Min(value = 1, message = "id must be greater than or equal to 1") @Max(value = 1000, message = "id must be lower than or equal to 1000") final Long id) {
		return new ResponseEntity<>(subjectService.findById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<SubjectDto> insert(@RequestBody SubjectDto subjectDto, HttpServletRequest request) throws SQLIntegrityConstraintViolationException {
		subjectDto = subjectService.insert(subjectDto);
		return new ResponseEntity<>(subjectDto, HttpStatus.OK);
	}

	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<SubjectDto> findBy(@RequestBody SubjectDto subjectDto, HttpServletRequest request) {
		SubjectDto subject = subjectService.findByNamedQuery(subjectDto.getName());

		return new ResponseEntity<>(subject, HttpStatus.OK);

	}
	@PutMapping("{id}")
	public SubjectDto Update(@RequestBody SubjectDto c, @PathVariable final Long id) throws SQLIntegrityConstraintViolationException {

		return subjectService.update(c, id);
	}
	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<SubjectDto>> pagination(@RequestParam final int page, @RequestParam final int num,
			HttpServletRequest request) {
		return new ResponseEntity<List<SubjectDto>>(subjectService.getPage(page, num), HttpStatus.OK);
	}

	@GetMapping
	@RequestMapping(path = "totalCount")
	public ResponseEntity<Integer> totalCount(HttpServletRequest request) {

		return new ResponseEntity<Integer>(subjectService.getTotalCount(), HttpStatus.OK);
	}

	
}
