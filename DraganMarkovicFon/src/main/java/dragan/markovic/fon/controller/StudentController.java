package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.model.StudentDto;
import dragan.markovic.fon.service.CityService;
import dragan.markovic.fon.service.StudentService;
import dragan.markovic.fon.util.St;

@Controller
@RequestMapping(value = "student")
@SessionAttributes(value = "studentForm")
public class StudentController {

	@Autowired
	private StudentService studentService;
	@Autowired
	CityService cityService;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		// binder.setValidator(validator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));

		St.DATE_FMT.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(St.DATE_FMT, false));

	}

	@ModelAttribute(value = "studentForm")
	public StudentDto createStudentDtoModel() {
		StudentDto s = new StudentDto();
		s.setFirstname("studentForma");
		s.setCurrentYearOfStudy(1);
		s.setCity(new CityDto());
		return new StudentDto();
	}

//	@ModelAttribute(value = "cities")
	public List<CityDto> getAllCities(Model model) {
		return cityService.getAll();
	}

	@ModelAttribute(value = "numberList")
	public List<Integer> populateNumberList() {

		return Arrays.asList(1, 2, 3, 4, 5, 6, 7);
	}

	@GetMapping
	public String home(Model modelAndView) {
		return "student/viewStudents";
	}

	@GetMapping(value = "/")
	public String index(Model model) {
		return "redirect:/student";
	}

	@GetMapping(value = "/add")
	public String add(Model modelAndView) {

		modelAndView.addAttribute("studentForm", new StudentDto());
		modelAndView.addAttribute("cities", cityService.getAll());
		modelAndView.addAttribute("numberList", populateNumberList());
		return "student/addStudent";
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(@Valid @ModelAttribute("studentForm") StudentDto studentDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("cities", cityService.getAll());

		if (errors.hasErrors()) {
			modelAndView.addObject("css", "danger");

			modelAndView.addObject("msg", "There is something wrong..");
			modelAndView.setViewName("student/addStudent");
		} else {
			System.out.println(studentDto.toString());
			// studentDto.getCityDto().getId();
			modelAndView.setViewName("student/add_confirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "save")
	public String save(@ModelAttribute(name = "studentForm") StudentDto studentDto,
			@RequestParam(name = "action") final String action, SessionStatus sessionStatus)
			throws SQLIntegrityConstraintViolationException {

		System.out.println(studentDto.toString());
		if (action.equalsIgnoreCase("save")) {

			studentService.insert(studentDto);
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("cancel")) {
			sessionStatus.setComplete();
		}
		if (action.equalsIgnoreCase("change")) {
			return ("student/addStudent");
		}
		if (action.equalsIgnoreCase("update")) {
			studentService.update(studentDto, studentDto.getId());
			sessionStatus.setComplete();
		}

		return "redirect:/viewStudents";
	}

	@GetMapping("/update/{id}")
	public String showFormForUpdate(@PathVariable("id") final int id, Model model) throws RecordNotFoundException {
		model.addAttribute("studentForm", studentService.findById((long) id));
		model.addAttribute("cities", cityService.getAll());
		model.addAttribute("numberList", populateNumberList());
		return "student/addStudent";
	}

	// delete user with id
	@GetMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") final int id, final RedirectAttributes redirectAttributes) {

		studentService.deleteById((long) id);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "Student " + id + " is deleted!");
		return "redirect:/student";

	}
	// show entity for id

	@GetMapping(value = "{id}")
	public String show(@PathVariable("id") final int id, Model model) {

		StudentDto s = studentService.findById((long) id);
		if (s == null) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "This not found");
		} else {
			s.setCity(cityService.findById((long) s.getCity().getId()));
		}
		model.addAttribute("studentForm", s);

		return "student/show";

	}
}
