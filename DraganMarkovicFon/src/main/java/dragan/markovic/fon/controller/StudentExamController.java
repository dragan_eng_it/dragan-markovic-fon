package dragan.markovic.fon.controller;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import dragan.markovic.fon.exception.MyValidationException;
import dragan.markovic.fon.model.StudentDto;
import dragan.markovic.fon.model.StudentExamDto;
import dragan.markovic.fon.service.ExamService;
import dragan.markovic.fon.service.StudentExamService;
import dragan.markovic.fon.service.StudentService;


@Controller
@RequestMapping(value = "/studentExam")
public class StudentExamController {
	private final ExamService examService;
	private final StudentService studentService;
	private final StudentExamService studentExamService;

	@Autowired
	StudentExamController(ExamService examService,StudentService studentService,StudentExamService studentExamService) {
		this.examService = examService;
		this.studentService = studentService;
		this.studentExamService = studentExamService;
	}

	@GetMapping
	public String home() {
		return "studentExam/home";
	}

	@GetMapping(value = "add")
	public ModelAndView add() {

		ModelAndView modelAndView = new ModelAndView("studentExams/addStudentExams");
		modelAndView.addObject("studentExamDto", new StudentExamDto());
		 modelAndView.addObject("students", getAllStudents());
		return modelAndView;
	}
	
	@ModelAttribute(name = "studentExamDto")
	private StudentExamDto getStudentExamDto() {
		return new StudentExamDto();
	}
	
	@ModelAttribute(name = "students")
	private List<StudentDto> getAllStudents() {

		return studentService.getAll();
	}

	@PostMapping(value = "confirm")
	public ModelAndView confirm(
			@Valid @ModelAttribute(name = "studentExamDto") StudentExamDto studentExamDto, Errors errors) {

		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			modelAndView.setViewName("studentExams/addstudentExam");
		} else {
			modelAndView.setViewName("studentExam/add_confirm");
		}

		return modelAndView;
	}

	@PostMapping(value = "save")
	public String save(
			@ModelAttribute(name="studentExamDto")StudentExamDto studentExamDto, 
			
			SessionStatus sessionStatus) throws MyValidationException, SQLIntegrityConstraintViolationException {
			studentExamService.insert(studentExamDto);
			sessionStatus.setComplete();
		
		return "redirect:/studentExams/addStudentExam";
	}

	@ExceptionHandler(MyValidationException.class)
	public ModelAndView exceptionHandler(MyValidationException validationException) {
		ModelAndView modelAndView = new ModelAndView("studentExam/addStudentExam");
		modelAndView.addObject("errorMessage", validationException.getMessage());
		modelAndView.addObject("studentExamDto",getStudentExamDto());
		return modelAndView;
	}
	
	
}
