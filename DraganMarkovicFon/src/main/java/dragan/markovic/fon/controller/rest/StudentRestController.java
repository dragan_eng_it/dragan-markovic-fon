package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.StudentDto;
import dragan.markovic.fon.service.StudentService;

@RestController
@RequestMapping(path = "/api/student")
public class StudentRestController {
	
	@Autowired
	private StudentService studentService;
	
	
	
	@GetMapping
	public ResponseEntity<List<StudentDto>> getAll(HttpServletRequest request) {
		return new ResponseEntity<List<StudentDto>>(studentService.getAll(), HttpStatus.OK);
	}

	
	
	@GetMapping("/{id}")
	public ResponseEntity<StudentDto> getById( @PathVariable final  Long id) {
		return new ResponseEntity<>(studentService.findById(id), HttpStatus.OK);
	}
	@PutMapping("{id}")
	public StudentDto Update(@RequestBody StudentDto studentDto, @PathVariable final Long  id) throws SQLIntegrityConstraintViolationException {

		return studentService.update(studentDto, id);
	}
	@PostMapping
	public ResponseEntity<StudentDto> insert(@RequestBody StudentDto studentDto, HttpServletRequest request) throws SQLIntegrityConstraintViolationException {
		return new ResponseEntity<>(studentService.insert(studentDto), HttpStatus.OK);
	}

	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<StudentDto> findBy(@RequestBody StudentDto studentDto, HttpServletRequest request) {

		return new ResponseEntity<>(studentService.findByNamedQuery(studentDto.getIndexnumber()), HttpStatus.OK);

	}

	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<StudentDto>> pagination(@RequestParam final int page, @RequestParam final int num,
			HttpServletRequest request) {
		return new ResponseEntity<List<StudentDto>>(studentService.getPage(page, num), HttpStatus.OK);
	}

	@GetMapping
	@RequestMapping(path = "totalCount")
	public ResponseEntity<Integer> totalCount(HttpServletRequest request) {

		return new ResponseEntity<Integer>(studentService.getTotalCount(), HttpStatus.OK);
	}
	
}
