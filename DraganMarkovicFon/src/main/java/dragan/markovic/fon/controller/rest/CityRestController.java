package dragan.markovic.fon.controller.rest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.service.CityService;

@RestController
@RequestMapping(path = "api/city")
public class CityRestController {
	@Autowired
	private CityService cityService;

	@GetMapping
	public List<CityDto> getAll() {
		
		return cityService.getAll();
	}
	@PostMapping
	public CityDto create(@RequestBody CityDto cityDto) throws SQLIntegrityConstraintViolationException {
		return cityService.insert(cityDto);
	}
	
	@GetMapping("{id}")
	public CityDto getById(
			@PathVariable @Min(value = 1, message = "id must be greater than or equal to 1") @Max(value = 1000, message = "id must be lower than or equal to 1000") final Long id) {
		return cityService.findById(id);
	}
	
	@PostMapping
	@RequestMapping(path = "findBy")
	public ResponseEntity<CityDto> findBy(@RequestBody CityDto cityDto, HttpServletRequest request) {
		return new ResponseEntity<>(cityService.findByNamedQuery(cityDto.getName()), HttpStatus.OK);
	}

	@PutMapping("{id}")
	public CityDto Update(@RequestBody CityDto cityDto, @PathVariable final Long id) throws SQLIntegrityConstraintViolationException {

		return cityService.update(cityDto, id);
	}

	@DeleteMapping("{id}")
	void delete(@PathVariable final Long id) {
		cityService.deleteById(id);
	}
	@GetMapping
	@RequestMapping(path = "pages")
	public ResponseEntity<List<CityDto>> pagination(@RequestParam final int page, @RequestParam final int num,
			HttpServletRequest request) {
		List<CityDto> listOfSub = cityService.getPage(page, num);

		return new ResponseEntity<List<CityDto>>(listOfSub, HttpStatus.OK);
	}
	@GetMapping
	@RequestMapping(path = "totalCount")
	public int totalCount(HttpServletRequest request) {

		return cityService.getTotalCount();
	}
}

