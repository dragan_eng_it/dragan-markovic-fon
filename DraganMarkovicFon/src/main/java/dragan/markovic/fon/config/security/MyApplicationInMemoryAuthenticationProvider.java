package dragan.markovic.fon.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import dragan.markovic.fon.controller.security.MyApplicationInMemoryPrincipal;
import dragan.markovic.fon.controller.security.service.impl.MyApplicationInMemoryUserDetailsService;


//@Component
public class MyApplicationInMemoryAuthenticationProvider implements AuthenticationProvider {
	UserDetailsService userDetailsService=new MyApplicationInMemoryUserDetailsService();

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String v1 = passwordEncoder.encode("admin");

		System.out.println("EQUALS: " + passwordEncoder.matches("admin", v1));

		String username = authentication.getPrincipal().toString();
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);

		UsernamePasswordAuthenticationToken token = (UsernamePasswordAuthenticationToken) authentication;

		if (userDetails instanceof MyApplicationInMemoryPrincipal) {
			System.out.println("token.getCredentials().toString() "+token.getCredentials().toString());
			
			if (passwordEncoder.matches(token.getCredentials().toString(), userDetails.getPassword())) {
				System.out.println("===================SUCCESS LOGIN  =======================");

				MyApplicationInMemoryPrincipal inMemoryPrincipal = (MyApplicationInMemoryPrincipal) userDetails;
				UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userDetails,
						new MyApplicationInMemoryPrincipal(inMemoryPrincipal.getUsername(),
								inMemoryPrincipal.getPassword()),
						userDetails.getAuthorities());
				return auth;

			} else {
				System.out.println("===================NOT SUCCESS LOGIN ====================");
			}
		}

		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
