package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonAutoDetect;
@JsonAutoDetect
public class  TitleDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String name;

	public TitleDto() {
	}


	public TitleDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public boolean isNew() {
		return valueOf(this.id == 0);
	}
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TitleDto [id=" + id + ", name=" + name + "]";
	}


	public TitleDto(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

}