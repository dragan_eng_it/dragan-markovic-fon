package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import dragan.markovic.fon.enums.Semester;
import dragan.markovic.fon.util.St;

public class SubjectDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	@NotNull
	@Length(max = 30)
	private String name;
	@Length(max = 200)

	private String description;

	private Semester semester;
	@Min(1) @Max(10)
	private int yearOfStudy;

	public SubjectDto() {
		super();
		this.semester=Semester.SUMMER;
	}

	public boolean isNew() {
		return valueOf(this.id == 0);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Semester getSemester() {
		return semester;
	}


	public void setSemester(Semester semester) {
		this.semester = semester;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	


	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	@Override
	public String toString() {
		return "id=" + id + ", description=" + description + ", name=" + name + ", semester=" + semester
				+ ", yearOfStudy=" + yearOfStudy + "]";
	}



	
}
