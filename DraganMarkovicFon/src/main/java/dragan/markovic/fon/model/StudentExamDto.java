package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;
import java.time.LocalDate;

 
public class StudentExamDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	
	private LocalDate date;
	private StudentDto student;
	private ExamDto subjectProfessor;
	public StudentExamDto() {
	}

	public boolean isNew() {
		return valueOf(this.id == 0);
	}
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return this.date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public StudentDto getStudent() {
		return this.student;
	}

	public void setStudent(StudentDto student) {
		this.student = student;
	}

	public ExamDto getSubjectProfessor() {
		return this.subjectProfessor;
	}

	public void setSubjectProfessor(ExamDto subjectProfessor) {
		this.subjectProfessor = subjectProfessor;
	}

	@Override
	public String toString() {
		return "StudentExamDto [id=" + id + ", date=" + date + ", student=" + student + ", subjectProfessor="
				+ subjectProfessor + "]";
	}

}