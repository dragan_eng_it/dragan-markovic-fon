package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import dragan.markovic.fon.util.St;

public class CityDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	@NotNull
	@Length(max=30)

	private String name;
	@NotNull
	@Length(max=30)

	private String number;
	public CityDto() {
		super();
	}
	
	
	public CityDto(long id,  String name, String number) {
		super();
		this.id = id;
		this.name = name;
		this.number = number;
	}
	public boolean isNew() {
		return valueOf(this.id == 0);
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	@Override
	public String toString() {
		return "CityDto [id=" + id + ", name=" + name + ", number=" + number + "]";
	}
	
	
}