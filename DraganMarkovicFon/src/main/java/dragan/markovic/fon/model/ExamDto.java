package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Convert;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dragan.markovic.fon.util.LocalDateAttributeConverter;
import dragan.markovic.fon.util.LocalDateDeserializer;
import dragan.markovic.fon.util.LocalDateSerializer;
import dragan.markovic.fon.util.St;

public class ExamDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@NotNull
	@Convert(converter = LocalDateAttributeConverter.class) 
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate examdate;
	private ProfessorDtoForExam professor;
	private SubjectDtoForExam subject;
	
	public ExamDto() {
		
	}

	public boolean isNew() {
		return valueOf(this.id == 0);
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public LocalDate getExamdate() {
		return examdate;
	}


	public void setExamdate(LocalDate examdate) {
		this.examdate = examdate;
	}


	public ProfessorDtoForExam getProfessor() {
		return professor;
	}


	public void setProfessor(ProfessorDtoForExam professor) {
		this.professor = professor;
	}


	public SubjectDtoForExam getSubject() {
		return subject;
	}


	public void setSubject(SubjectDtoForExam subject) {
		this.subject = subject;
	}


	@Override
	public String toString() {
		return "ExamDto [id=" + id + ", examdate=" + examdate + ", professor=" + professor + ", subject=" + subject
				+ "]";
	}


	

	
}