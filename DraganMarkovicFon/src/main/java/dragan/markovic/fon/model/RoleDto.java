package dragan.markovic.fon.model;

import java.io.Serializable;

import static dragan.markovic.fon.util.St.valueOf;


public class RoleDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;

	public RoleDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public RoleDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public boolean isNew() {
		return valueOf(this.id == 0);
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "RoleDto [id=" + id + ", name=" + name + "]";
	}

	
}
