package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dragan.markovic.fon.enums.Semester;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubjectDtoForExam implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String name;
	private Semester semester;
	private int yearOfStudy;

	public SubjectDtoForExam() {
		super();
	}

	public boolean isNew() {
		return valueOf(this.id == 0);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public Semester getSemester() {
		return semester;
	}


	public void setSemester(Semester semester) {
		this.semester = semester;
	}


	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	@Override
	public String toString() {
		return "SubjectDto [id=" + id + ", name=" + name + ", semester=" + semester
				+ ", yearOfStudy=" + yearOfStudy + "]";
	}


	


}
