package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dragan.markovic.fon.util.St;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfessorDtoForExam implements Serializable{
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String firstname;
	private String lastname;
	
	@Email
	private String email;
	public ProfessorDtoForExam() {
	}
	
	
	public ProfessorDtoForExam(long id, String firstname, String lastname,  String email) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}


	public boolean isNew() {
		return valueOf(this.id == 0);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "ProfessorDtoForExam. [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email="
				+ email + "]";
	}
	
	
	

	
}