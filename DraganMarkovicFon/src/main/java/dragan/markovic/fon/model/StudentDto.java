package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;


import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonInclude;

import dragan.markovic.fon.util.St;
import dragan.markovic.fon.validator.Phone;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class StudentDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	@NotNull
	@Length(max=10)
	private String indexnumber;
	@NotNull
	@Pattern(regexp="^[A-Za-z]*$")
	@Length(max=30)
	private String firstname;

	@NotNull
	@Length(min=3,max=30)
	@Pattern(regexp="^[A-Za-z]*$")
	private String lastname;
	@Length(max=50)
	private String address;
	@NotNull
	@Min(1) @Max(10)
	private int currentYearOfStudy;
	@Length(max=30)
	@Email
	private String email;
	
	private CityDto city;
	@Phone
	private String phone;
	
	public StudentDto() {
		this.setCity(new CityDto());
	}

	public boolean isNew() {
		return valueOf(this.id == 0);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIndexnumber() {
		return indexnumber;
	}

	public void setIndexnumber(String indexnumber) {
		this.indexnumber = indexnumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexnumber=" + indexnumber + ", firstname=" + firstname + ", lastname="
				+ lastname + ", address=" + address + ", currentYearOfStudy=" + currentYearOfStudy + ", email=" + email
				+ ", city=" + city + ", phone=" + phone + "]";
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	
	
	

	}