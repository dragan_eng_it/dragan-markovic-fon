package dragan.markovic.fon.model;

import static dragan.markovic.fon.util.St.valueOf;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Convert;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dragan.markovic.fon.util.LocalDateAttributeConverter;
import dragan.markovic.fon.util.LocalDateDeserializer;
import dragan.markovic.fon.util.LocalDateSerializer;
import dragan.markovic.fon.util.St;
import dragan.markovic.fon.validator.Phone;



public class ProfessorDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private long id;
	@NotNull
	@Size(min=3, max=30)
	@Pattern(regexp="^[A-Za-z]*$")
	private String firstname;
	@NotNull
	@Size(min=3, max=30)
	@Pattern(regexp="^[A-Za-z]*$")
	private String lastname;
	@Size( max=60)
	private String address;
	@Size(max=30)
	@NotEmpty @Email
	private String email;
	@Phone
	private String phone;
	//@JsonFormat(pattern="dd/MM/yyyy")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Convert(converter = LocalDateAttributeConverter.class) 
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate reelectiondate;
	private TitleDto title;
	private CityDto city;
	public ProfessorDto() {
		this.setCity(new CityDto());
		this.setTitle( new TitleDto());
	}
	
	public boolean isNew() {
		return valueOf(this.id == 0);
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public LocalDate getReelectiondate() {
		return reelectiondate;
	}
	public void setReelectiondate(LocalDate reelectiondate) {
		this.reelectiondate = reelectiondate;
	}
	
	public TitleDto getTitle() {
		return title;
	}
	public void setTitle(TitleDto title) {
		this.title = title;
	}
	public CityDto getCity() {
		return city;
	}
	public void setCity(CityDto city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", address=" + address
				+ ", email=" + email + ", phone=" + phone + ", reelectiondate=" + reelectiondate + ", title=" + title
				+ ", city=" + city + "]";
	}

	
}