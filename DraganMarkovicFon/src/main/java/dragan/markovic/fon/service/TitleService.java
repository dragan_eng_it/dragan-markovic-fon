package dragan.markovic.fon.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.TitleEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.TitleMapper;
import dragan.markovic.fon.model.TitleDto;
import dragan.markovic.fon.repository.IRepository;

@Service
@Transactional
public class TitleService {

	@Autowired
	private IRepository<TitleEntity, Long> titleRepository;

	@Autowired
	private TitleMapper modelMapper;

	public List<TitleDto> getAll() {
		List<TitleDto> titles = new ArrayList<TitleDto>();
		List<TitleEntity> clist = titleRepository.getAll();
		for (TitleEntity c : clist) {
			titles.add(new TitleDto(c.getId(), c.getName()));
		}
		return titles;
	}

	public TitleDto findById(final long id) {
		Optional<TitleEntity> t = titleRepository.findById(id);

		if (t.isPresent()) {
			return modelMapper.map(t.get(), TitleDto.class);

		} else {
			throw new RecordNotFoundException("not found title id )" + id);
		}

	}

	public void deleteById(final Long id) {
		titleRepository.deleteById(id);

	}

	public TitleDto insert(TitleDto titleDto) {

		TitleEntity titleEntity = modelMapper.map(titleDto, TitleEntity.class);
		TitleEntity newTitle = titleRepository.create(titleEntity);
		return modelMapper.map(newTitle, TitleDto.class);
	}

	public TitleDto update(TitleDto titleDto,final long id) {

		TitleEntity titleEntity = modelMapper.map(titleDto, TitleEntity.class);
		titleEntity.setId(id);
		titleEntity = titleRepository.update(titleEntity);
		return modelMapper.map(titleEntity, TitleDto.class);
	}

	public List<TitleDto> getPage(final int page,final int num) {
		List<TitleDto> listOfSub = getAll();
		List<TitleDto> pageSub = new ArrayList<TitleDto>();
		int first = (page - 1) * num;
		int last = (listOfSub.size() < page * num) ? listOfSub.size() : page * num;
		for (int i = first; i < last; i++) {
			pageSub.add(listOfSub.get(i));
		}
		return pageSub;
	}

	public int getTotalCount() {
		int total = titleRepository.getToatalCount();
		return total;
	}

	public TitleDto findByNamedQuery(String id) {
		// TODO Auto-generated method stub
		return null;
	}
}
