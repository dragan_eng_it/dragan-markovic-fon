package dragan.markovic.fon.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.UserEntity;
import dragan.markovic.fon.mapper.UserMapper;
import dragan.markovic.fon.model.UserDto;
import dragan.markovic.fon.repository.IRepository;


@Service
@Transactional
public class UserService{
	@Autowired
	private  IRepository<UserEntity,Long>userRepository;
	
	@Qualifier(value="userMapper")
	@Autowired
	private  UserMapper userConverter;
	
	
	 
	public UserDto findByUsername(final String email) {
		Optional<UserEntity> userEntityOpt = userRepository.findByNamedQuery(email);
		if(userEntityOpt.isPresent()) {
		return userConverter.map(userEntityOpt.get(),UserDto.class);
		}
		else {
			return new UserDto();
			
		}
	}

}
