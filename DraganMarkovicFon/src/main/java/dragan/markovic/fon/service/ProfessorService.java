package dragan.markovic.fon.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.ProfessorEntity;
import dragan.markovic.fon.entity.TitleEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.ProfessorMapper;
import dragan.markovic.fon.model.ProfessorDto;
import dragan.markovic.fon.model.ProfessorDtoForExam;
import dragan.markovic.fon.repository.IRepository;
import dragan.markovic.fon.entity.CityEntity;

@Service
@Transactional
public class ProfessorService   {

	@Autowired
	private IRepository<ProfessorEntity,Long> professorRepository;
	
	@Autowired
	private IRepository<CityEntity,Long> cityRepository;
	@Autowired
	private IRepository<TitleEntity,Long> titleRepository;	
	
	@Autowired
	@Qualifier("professorMapper")
	private ProfessorMapper modelMapper;

	 
	public List<ProfessorDto> getAll() {
		List<ProfessorDto> listOfProfessorDto = professorRepository.getAll().stream()
				.map(r -> modelMapper.makeJson(r).buildProfessorDto()).collect(Collectors.toList());

		return listOfProfessorDto;
	}
	public List<ProfessorDtoForExam> getAllforExam() {
		List<ProfessorDtoForExam> listOfProfessorDto = professorRepository.getAll().stream()
				.map(r -> modelMapper.makeJson(r).buildProfessorDtoForExam()).collect(Collectors.toList());

		return listOfProfessorDto;
	}
	 
	public ProfessorDto findById(final long id) {

		ProfessorEntity prof = professorRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("Professor id '" + id + "' does no exist"));
		return modelMapper.makeJson(prof).buildProfessorDto();
	}

	 
	public void deleteById(final Long id) {
		professorRepository.deleteById(id);

	}

	 
	public ProfessorDto update(ProfessorDto professorDto,final long id) {
		Optional<ProfessorEntity> p = professorRepository.findById(id);
		if (p.isPresent()) {
			ProfessorEntity newProfessor = modelMapper.map(professorDto, ProfessorEntity.class);
			newProfessor.setId(id);
			professorRepository.update(newProfessor);
			professorDto.setId(id);
			return professorDto;
		} else {
			throw new PersistenceException("Nema profesora sa id " + id);

		}

	}

	 
	public ProfessorDto insert(ProfessorDto professorDto) {
		
		
		ProfessorEntity newProfessor = modelMapper.map(professorDto, ProfessorEntity.class);
		
		Optional<CityEntity> city=cityRepository.findById(professorDto.getCity().getId());
		if (city.isPresent()) {
			 Optional<TitleEntity> title = titleRepository.findById(professorDto.getTitle().getId());
			 if(title.isPresent()) {
				 newProfessor.setCity(city.get());
				 newProfessor.setTitle(title.get());
				 ProfessorEntity newEnt = professorRepository.create(newProfessor);
				 return modelMapper.map(newEnt,ProfessorDto.class);
			 }else{
				 throw new RecordNotFoundException("title no exists");
				 
			 }
		}else {
			throw new RecordNotFoundException("city no exists");
		}
	}

	 
	public List<ProfessorDto> getPage(final int page,final  int num) {
		
		List<ProfessorEntity> listOf = professorRepository.findByPage( page, num);
		List<ProfessorDto> pageSub =listOf.stream().map(s->modelMapper.makeJson(s).buildProfessorDto()).collect(Collectors.toList());
		return pageSub;
	}

	 
	public int getTotalCount() {
		int total = professorRepository.getToatalCount();
		return total;
	}

	 
	public ProfessorDto findByNamedQuery(final String name) {
		return modelMapper.map(professorRepository.findByNamedQuery(name), ProfessorDto.class);
	}
}

