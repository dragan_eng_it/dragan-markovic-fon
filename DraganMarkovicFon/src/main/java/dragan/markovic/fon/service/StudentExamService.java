package dragan.markovic.fon.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.StudentExamEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.StudentExamMapper;
import dragan.markovic.fon.model.StudentExamDto;
import dragan.markovic.fon.repository.IRepository;

@Service
@Transactional
public class StudentExamService  {

	
	@Autowired
	private IRepository<StudentExamEntity,Long> studentExamRepository;
	@Autowired
	
	private StudentExamMapper studentExamMapper;
	
	 
	public List<StudentExamDto> getAll() {
		List<StudentExamEntity> listOfStudentExamEntity = studentExamRepository.getAll();
		
		List<StudentExamDto> listOfStudentExamDto=new ArrayList<>();
		for ( StudentExamEntity pf: listOfStudentExamEntity ) {
			StudentExamDto studentExamDto = studentExamMapper.toDto(studentExamMapper.toJson(pf));
			listOfStudentExamDto.add(studentExamDto);
		}
		return listOfStudentExamDto;
	}

	 
	public StudentExamDto findById(final long id) {

		StudentExamEntity role = studentExamRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("Role id '" + id + "' does no exist"));
		return studentExamMapper.map(role, StudentExamDto.class);
	}

	 
	public void deleteById(final Long id) {
		studentExamRepository.deleteById(id);

	}

	 
	public StudentExamDto update(StudentExamDto studentExamDto,final long id) {
		Optional<StudentExamEntity> role = studentExamRepository.findById(id);
		if (role.isPresent()) {
			StudentExamEntity newStudentExam = studentExamMapper.map(studentExamDto, StudentExamEntity.class);
			newStudentExam.setId(id);
			studentExamRepository.update(newStudentExam);
			studentExamDto.setId(newStudentExam.getId());
			return studentExamDto;
		} else {
			insert(studentExamDto);

		}
		return studentExamDto;

	}

	 
	public StudentExamDto insert(StudentExamDto studentExamDto) {
		StudentExamEntity newStudentExam = studentExamRepository.create(studentExamMapper.map(studentExamDto, StudentExamEntity.class));
		return studentExamMapper.map(newStudentExam, StudentExamDto.class);
	}

	
	public List<StudentExamDto> getPage(final int page,final int num) {
		List<StudentExamDto> listOfSub = getAll();
		List<StudentExamDto> pageSub = new ArrayList<StudentExamDto>();
		int first = (page-1)*num;
		int last = (listOfSub.size() < page*num) ? listOfSub.size() : page*num; 
		for(int i = first; i < last; i++) {
			
			pageSub.add(listOfSub.get(i));
		}
		
		return pageSub;
	}

	 
	public int getTotalCount() {
		int total = studentExamRepository.getToatalCount();
		return total;
	}


	 
	public StudentExamDto findByNamedQuery(final String id) {
		// TODO Auto-generated method stub
		return null;
	}

	

	

	
}
