package dragan.markovic.fon.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.SubjectEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.SubjectMapper;
import dragan.markovic.fon.model.SubjectDto;
import dragan.markovic.fon.repository.IRepository;

@Service
@Transactional
public class SubjectService   {

	@Autowired 
	private IRepository<SubjectEntity, Long> subjectRepository;
	@Autowired
	@Qualifier("subjectMapper")
	private SubjectMapper modelMapper;

	 
	public List<SubjectDto> getAll() {
		List<SubjectDto> listOfSubjectDto = subjectRepository.getAll().stream()
				.map(r -> modelMapper.makeJson(r).buildSubjectDto()).collect(Collectors.toList());

		return listOfSubjectDto;
	}

	 
	public SubjectDto findById(final long id) {

		SubjectEntity role = subjectRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("Role id '" + id + "' does no exist"));
		return modelMapper.makeJson(role).buildSubjectDto();
	}

	 
	public void deleteById(final Long id) {
		subjectRepository.deleteById(id);

	}

	 
	public SubjectDto update(SubjectDto subjectDto,final long id) {
		Optional<SubjectEntity> r = subjectRepository.findById(id);
		if (r.isPresent()) {
			SubjectEntity newSubject = modelMapper.map(subjectDto, SubjectEntity.class);
			newSubject.setId(id);
			subjectRepository.update(newSubject);
			subjectDto.setId(newSubject.getId());
			return subjectDto;
		} else {
			insert(subjectDto);

		}
		return subjectDto;

	}

	 
	public SubjectDto insert(SubjectDto subjectDto) {

		SubjectEntity newS = modelMapper.map(subjectDto, SubjectEntity.class);
		//newS.setSemester(sem);
		subjectRepository.create(newS);
		return modelMapper.map(newS, SubjectDto.class);

	}

	 
	public List<SubjectDto> getPage(final int page,final int num) {
		List<SubjectEntity> listOfSub = subjectRepository.findByPage(page, num);
		List<SubjectDto> pageSub = listOfSub.stream().map(s -> modelMapper.makeJson(s).buildSubjectDto())
				.collect(Collectors.toList());
		return pageSub;
	}

	 
	public int getTotalCount() {
		int total = subjectRepository.getToatalCount();
		return total;
	}
	 
	public SubjectDto findByNamedQuery(final String name) {
		Optional<SubjectEntity> subj = subjectRepository.findByNamedQuery(name);
		if (subj.isPresent()) {
			return modelMapper.makeJson(subj.get()).buildSubjectDto();
		} else {
			throw new RecordNotFoundException("subject not find in namedquery" + name);
		}
	}

	
}
