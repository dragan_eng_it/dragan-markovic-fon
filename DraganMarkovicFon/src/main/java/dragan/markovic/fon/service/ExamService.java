package dragan.markovic.fon.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.ExamEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.ExamMapper;
import dragan.markovic.fon.model.ExamDto;
import dragan.markovic.fon.repository.IRepository;

@Service
@Transactional
public class ExamService {
	@Autowired
	private IRepository<ExamEntity, Long> examRepository;

	@Autowired
	@Qualifier("examMapper")
	private ExamMapper modelMapper;

	public List<ExamDto> getAll() {

		return examRepository.getAll().stream().map(role -> modelMapper.makeJson(role).buildExamDto())
				.collect(Collectors.toList());
	}

	public ExamDto findById(final long id) {

		ExamEntity prof = examRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("Exam id '" + id + "' does no exist"));
		return modelMapper.makeJson(prof).buildExamDto();
	}

	public void deleteById(Long id) {
		examRepository.deleteById(id);

	}

	public ExamDto update(ExamDto examDto,final long id) {
		Optional<ExamEntity> p = examRepository.findById(id);
		if (p.isPresent()) {
			ExamEntity newExam = modelMapper.map(examDto, ExamEntity.class);
			newExam.setId(id);
			examRepository.update(newExam);
			examDto.setId(id);
			return examDto;
		} else {
			throw new PersistenceException("Nema exama sa id " + id);

		}

	}


	public ExamDto insert(ExamDto examDto) {

		ExamEntity newExam = modelMapper.map(examDto, ExamEntity.class);

		ExamEntity newEnt = examRepository.create(newExam);
		return modelMapper.makeJson(newEnt).buildExamDto();
	}

	public List<ExamDto> getPage(final int page,final int num) {

		List<ExamDto> pageSub = examRepository.findByPage(page, num).stream().map(s -> modelMapper.makeJson(s).buildExamDto())
				.collect(Collectors.toList());
		return pageSub;
	}

	public int getTotalCount() {
		return (int) examRepository.getToatalCount();
	}

	public ExamDto findByNamedQuery(final String name) {
		return modelMapper.map(examRepository.findByNamedQuery(name), ExamDto.class);
	}
}
