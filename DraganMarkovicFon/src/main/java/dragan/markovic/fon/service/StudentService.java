package dragan.markovic.fon.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.StudentEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.StudentMapper;
import dragan.markovic.fon.model.StudentDto;
import dragan.markovic.fon.repository.IRepository;
import dragan.markovic.fon.entity.CityEntity;

@Service
@Transactional
public class StudentService {

	@Autowired
	private IRepository<StudentEntity, Long> studentRepository;
	@Autowired
	private StudentMapper modelMapper;

	@Autowired
	private IRepository<CityEntity, Long> cityRepository;

	public List<StudentDto> getAll() {
		List<StudentDto> listOfStudentDto = studentRepository.getAll().stream()
				.map(r -> modelMapper.makeJson(r).buildStudentDto()).collect(Collectors.toList());

		return listOfStudentDto;
	}

	public StudentDto findById(final long id) {

		StudentEntity role = studentRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("Student id '" + id + "' does not exist"));
		return modelMapper.makeJson(role).buildStudentDto();
	}

	public void deleteById(final Long id) {
		studentRepository.deleteById(id);

	}

	public StudentDto update(StudentDto studentDto,final long id) {

		Optional<StudentEntity> stud = studentRepository.findById(id);
		if (stud.isPresent()) {
			StudentEntity newStudent = modelMapper.map(studentDto, StudentEntity.class);
			newStudent.setId(id);
			studentRepository.update(newStudent);
			studentDto.setId(id);
			return studentDto;
		} else {

			throw new PersistenceException("Nema studenta sa id " + id);
		}

	}

	public StudentDto insert(StudentDto studentDto) {

		StudentEntity newstudent = modelMapper.map(studentDto, StudentEntity.class);
		Optional<CityEntity> city = cityRepository.findById(studentDto.getCity().getId());
		if (city.isPresent()) {

			newstudent.setCity(city.get());
			newstudent = studentRepository.create(newstudent);
			return modelMapper.makeJson(newstudent).buildStudentDto();

		} else {
			throw new RecordNotFoundException("city no exists");
		}
	}

	public List<StudentDto> getPage(final int page,final int num) {
		List<StudentEntity> listOfSub = studentRepository.findByPage(page, num);
		List<StudentDto> pageSub = listOfSub.stream().map(s -> modelMapper.makeJson(s).buildStudentDto())
				.collect(Collectors.toList());
		return pageSub;
	}

	public int getTotalCount() {
		int total = studentRepository.getToatalCount();
		return total;
	}

	public StudentDto findByNamedQuery(final String indexnumber) {
		Optional<StudentEntity> st = studentRepository.findByNamedQuery(indexnumber);
		if (st.isPresent()) {
			StudentDto s = new StudentDto();
			s.setId(st.get().getId());
			s.setFirstname(st.get().getFirstname());
			s.setLastname(st.get().getLastname());

			return s;
		} else {
			StudentDto s = new StudentDto();
			s.setFirstname("unknown");
			return s;
		}
	}

}
