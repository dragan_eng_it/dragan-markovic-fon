package dragan.markovic.fon.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.RoleEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.RoleMapper;
import dragan.markovic.fon.model.RoleDto;
import dragan.markovic.fon.repository.IRepository;

@Service
@Transactional
public class RoleService   {
	@Autowired
	private IRepository<RoleEntity,Long> roleRepository;
	@Autowired
	private RoleMapper roleMapper;

	 
	public List<RoleDto> getAll() {
		List<RoleDto> listOfRoleDto = roleRepository.getAll().stream()
				.map(role -> new RoleDto(role.getId(), role.getName())).collect(Collectors.toList());
		
		return listOfRoleDto;
	}

	 
	public RoleDto findById(final long id) {

		RoleEntity role = roleRepository.findById(id)
				.orElseThrow(() -> new RecordNotFoundException("Role id '" + id + "' does no exist"));
		return roleMapper.map(role, RoleDto.class);
	}

	 
	public void deleteById(final Long id) {
		roleRepository.deleteById(id);

	}

	 
	public RoleDto update(RoleDto roleDto,final long id) {
		Optional<RoleEntity> role = roleRepository.findById(id);
		if (role.isPresent()) {
			RoleEntity newRole = roleMapper.map(roleDto, RoleEntity.class);
			newRole.setId(id);
			roleRepository.update(newRole);
			roleDto.setId(newRole.getId());
			return roleDto;
		} else {
			insert(roleDto);

		}
		return roleDto;

	}

	 
	public RoleDto insert(RoleDto roleDto) {
		 RoleEntity newRole = roleRepository.create(roleMapper.map(roleDto, RoleEntity.class));
		return roleMapper.map(newRole, RoleDto.class);
	}
	 
	public List<RoleDto> getPage(int page, int num) {
		List<RoleDto> listOfSub = getAll();
		List<RoleDto> pageSub = new ArrayList<RoleDto>();
		int first = (page-1)*num;
		int last = (listOfSub.size() < page*num) ? listOfSub.size() : page*num; 
		for(int i = first; i < last; i++) {
			pageSub.add(listOfSub.get(i));
		}
		return pageSub;
	}

	 
	public int getTotalCount() {
		int total = roleRepository.getToatalCount();
		return total;
	}

	 
	public RoleDto findByNamedQuery(final String name) {
			return roleMapper.map(roleRepository.findByNamedQuery(name), RoleDto.class);
		}
	
}
