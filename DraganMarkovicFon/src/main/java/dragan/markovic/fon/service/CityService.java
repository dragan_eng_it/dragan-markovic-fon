package dragan.markovic.fon.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.repository.IRepository;
import dragan.markovic.fon.entity.CityEntity;
import dragan.markovic.fon.exception.RecordNotFoundException;
import dragan.markovic.fon.mapper.CityMapper;

@Service
@Transactional
public class CityService  {
	@Autowired
	private IRepository<CityEntity, Long> cityRepository;

	@Autowired
	@Qualifier("cityMapper")
	private CityMapper modelMapper;

	public CityService() {
		super();
	}

	public List<CityDto> getAll() {
		List<CityDto> cityList = cityRepository.getAll().stream()
				.map(r -> modelMapper.makeJson(r).buildCityDto()).collect(Collectors.toList());

		return cityList;
	}

	public CityDto findById(final long id) {
		Optional<CityEntity> ck = cityRepository.findById(id);

		if (ck.isPresent()) {
			
			CityDto cityDto = new CityDto(ck.get().getId(), ck.get().getName(), ck.get().getNumber());
			return cityDto;
		}else {
			throw new RecordNotFoundException("City with id "+id+" not found");
		}
		
	}

	public CityDto findByNamedQuery(final String name) {
		Optional<CityEntity> cit = cityRepository.findByNamedQuery(name);
		if (cit.isPresent()) {
			CityDto ci = new CityDto(cit.get().getId(), cit.get().getName(), cit.get().getNumber());
			return ci;
		} else {
			throw new RecordNotFoundException("this city not exists");
		}

	}

	public void deleteById(final Long id) {
		cityRepository.deleteById(id);

	}

	public CityDto insert(CityDto cityDto) {

		CityEntity cityEntity = modelMapper.map(cityDto, CityEntity.class);
		CityEntity newCity = cityRepository.create(cityEntity);
		return modelMapper.map(newCity, CityDto.class);
	}

	public CityDto update(CityDto cityDto,final long id) {

		CityEntity cityEntity = modelMapper.map(cityDto, CityEntity.class);
		cityEntity.setId(id);
		cityEntity = cityRepository.update(cityEntity);
		return modelMapper.map(cityEntity, CityDto.class);
	}

	public List<CityDto> getPage(final int page,final  int num) {

		List<CityEntity> listOfSub = cityRepository.findByPage(page, num);
		List<CityDto> pageSub = new ArrayList<CityDto>();
		
		for (CityEntity c:listOfSub) {
			pageSub.add(modelMapper.map(c, CityDto.class));
		}
		return pageSub;
	}

	public int getTotalCount() {
		return cityRepository.getToatalCount();
	}

}
