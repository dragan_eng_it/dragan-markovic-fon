package dragan.markovic.fon.enums;

import java.util.Arrays;

public enum Semester {
	SUMMER("SUMMER"), WINTER("WINTER");

	private String semester;

	Semester(String semester) {
		this.semester = semester;
	}

	public String getSemester() {
		return semester;
	}

	@Override
	public String toString() {
		return semester;
	}
	
	
}
