package dragan.markovic.fon.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "city")



@NamedQueries({
    @NamedQuery(name = "City.findAll", query = "SELECT c FROM CityEntity c")
    , @NamedQuery(name = "City.findById", query = "SELECT c FROM CityEntity c WHERE c.id = :id")
    , @NamedQuery(name = "City.findByNumber", query = "SELECT c FROM CityEntity c WHERE c.number = :number")
    , @NamedQuery(name = "City.findByName", query = "SELECT c FROM CityEntity c WHERE c.name LIKE :name"),
    @NamedQuery(name="City.findByPage", query="SELECT c FROM CityEntity c WHERE c.id > :id"),
    @NamedQuery(name = "City.findAllCount", query = "SELECT COUNT(c) FROM CityEntity c")
    })
public class CityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Long id;
	@NotNull
	@Size(min=3, max=30)

	private String name;
	@NotNull
	@Size(min=3, max=30)

	private String number;

	public CityEntity() {
	}

	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "CityEntity [id=" + id + ", name=" + name + ", number=" + number + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CityEntity other = (CityEntity) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}

}