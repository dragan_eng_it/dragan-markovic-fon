package dragan.markovic.fon.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Length;


@Entity
@Table(name="title")
@NamedQueries({
    @NamedQuery(name = "Title.findAll", query = "SELECT t FROM TitleEntity t")
    , @NamedQuery(name = "Title.findById", query = "SELECT t FROM TitleEntity t WHERE t.id = :id")
    , @NamedQuery(name = "Title.findByName", query = "SELECT t FROM TitleEntity t WHERE t.name = :name"),
	@NamedQuery(name = "Title.findAllCount", query = "SELECT COUNT(t) FROM TitleEntity t")})
public class TitleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@NotNull
	@NaturalId
	@Length(min = 3, max = 10)
	@Pattern(regexp="^[A-Za-z]*$")
	private String name;

	

	public TitleEntity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TitleEntity [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitleEntity other = (TitleEntity) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	

}