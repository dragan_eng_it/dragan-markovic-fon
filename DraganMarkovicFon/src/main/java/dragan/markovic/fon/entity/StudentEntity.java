package dragan.markovic.fon.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.NaturalId;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import dragan.markovic.fon.validator.Phone;


@Entity
@Table(name = "student")

@NamedQueries({
    @NamedQuery(name = "Student.findAll", query = "SELECT s FROM StudentEntity s")
    , @NamedQuery(name = "Student.findById", query = "SELECT s FROM StudentEntity s WHERE s.id = :id")
    , @NamedQuery(name = "Student.findByIndexNumber", query = "SELECT s FROM StudentEntity s WHERE s.indexnumber = :indexnumber")
    , @NamedQuery(name = "Student.findByCurrentYearOfStudy", query = "SELECT s FROM StudentEntity s WHERE s.currentYearOfStudy = :currentYearOfStudy"),
	@NamedQuery(name = "Student.findAllCount", query = "SELECT COUNT(s) FROM StudentEntity s")})

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@NotNull
	@Length(max=10)
	@NaturalId
	private String indexnumber;
	@NotNull
	@Pattern(regexp="^[A-Za-z]*$")
	@Length(max=30)
	private String firstname;

	@NotNull
	@Length(min=3,max=30)
	@Pattern(regexp="^[A-Za-z]*$")
	private String lastname;
	@Length(max=50)
	private String address;
	@NotNull
	@Min(1) @Max(10)
	private int currentYearOfStudy;
	@Length(max=30)
	@Email
	private String email;
	@Phone
	private String phone;

	@JoinColumn(name="cityId")
	@ManyToOne(optional = true)
	private CityEntity city;
;

	public StudentEntity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCurrentYearOfStudy() {
		return this.currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getIndexnumber() {
		return this.indexnumber;
	}

	public void setIndexnumber(String indexnumber) {
		this.indexnumber = indexnumber;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public CityEntity getCity() {
		return this.city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	

	@Override
	public String toString() {
		return "StudentEntity [id=" + id + ", address=" + address + ", currentYearOfStudy=" + currentYearOfStudy
				+ ", email=" + email + ", firstname=" + firstname + ", indexnumber=" + indexnumber + ", lastname="
				+ lastname + ", phone=" + phone + ", city=" + city  + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + currentYearOfStudy;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((indexnumber == null) ? 0 : indexnumber.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentEntity other = (StudentEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (currentYearOfStudy != other.currentYearOfStudy)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (indexnumber == null) {
			if (other.indexnumber != null)
				return false;
		} else if (!indexnumber.equals(other.indexnumber))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		return true;
	}

}