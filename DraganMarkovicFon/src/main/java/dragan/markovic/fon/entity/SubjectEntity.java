package dragan.markovic.fon.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import dragan.markovic.fon.enums.Semester;


@Entity
@Table(name = "subject")

@NamedQueries({
@NamedQuery(name = "Subject.findAll", query = "SELECT s FROM SubjectEntity s"),
@NamedQuery(name = "Subject.findById", query = "SELECT s FROM SubjectEntity s WHERE s.id = :id"),
@NamedQuery(name = "Subject.findByName", query = "SELECT s FROM SubjectEntity s WHERE s.name = :name"),
@NamedQuery(name = "Subject.findAllCount", query = "SELECT COUNT(s) FROM SubjectEntity s")})

public class SubjectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@NotNull
	@Length(max = 30)
	private String name;
	@Length(max = 200)
	private String description;
	
	//Using @Enumerated(EnumType.STRING) isn’t null safe. 
	@Enumerated(EnumType.STRING)
	//@Convert(converter = SemesterJpaConverter.class)  
	private Semester semester;
	
//	public enum Semester {
//		WINTER, SUMMER
//	}
	@Min(1) @Max(10)
	private short yearOfStudy;
	
	
	public SubjectEntity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public short getYearOfStudy() {
		return this.yearOfStudy;
	}

	public void setYearOfStudy(short yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	

	

	@Override
	public String toString() {
		return "SubjectEntity [id=" + id + ", description=" + description + ", name=" + name + ", semester=" + semester
				+ ", yearOfStudy=" + yearOfStudy  + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + yearOfStudy;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectEntity other = (SubjectEntity) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (yearOfStudy != other.yearOfStudy)
			return false;
		return true;
	}

}