package dragan.markovic.fon.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dragan.markovic.fon.util.LocalDateAttributeConverter;
import dragan.markovic.fon.util.LocalDateDeserializer;
import dragan.markovic.fon.util.LocalDateSerializer;

@Entity
@Table(name = "exam")
@NamedQueries({ @NamedQuery(name = "Exam.findAll", query = "SELECT e FROM ExamEntity e"),
		@NamedQuery(name = "Exam.findById", query = "SELECT e FROM ExamEntity e WHERE e.id = :id"),
		@NamedQuery(name = "Exam.findBySubject", query = "SELECT e FROM ExamEntity e WHERE e.subject = :subject"),
		@NamedQuery(name = "Exam.findByExamdate", query = "SELECT e FROM ExamEntity e WHERE e.examdate = :examdate"),
		@NamedQuery(name = "Exam.findAllCount", query = "SELECT COUNT(e) FROM ExamEntity e") })

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class ExamEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull
	@Future
	// @Column(name = "examdate", columnDefinition = "DATE DEFAULT CURRENT_DATE")
	@Convert(converter = LocalDateAttributeConverter.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate examdate;

	@JoinColumn(name = "professorId")
	@ManyToOne(optional = false)
	private ProfessorEntity professor;

	@JoinColumn(name = "subjectId")
	@ManyToOne(optional = false)
	private SubjectEntity subject;

	public ExamEntity() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getExamdate() {
		return examdate;
	}

	public void setExamdate(LocalDate examdate) {
		this.examdate = examdate;
	}

	public ProfessorEntity getProfessor() {
		return professor;
	}

	public void setProfessor(ProfessorEntity professor) {
		this.professor = professor;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "ExamEntity [id=" + id + ", examdate=" + examdate + ", professor=" + professor + ", subject=" + subject
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamEntity other = (ExamEntity) obj;
		if (professor == null) {
			if (other.professor != null)
				return false;
		} else if (!professor.equals(other.professor))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

}