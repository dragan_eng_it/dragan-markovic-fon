package dragan.markovic.fon.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dragan.markovic.fon.util.LocalDateDeserializer;
import dragan.markovic.fon.util.LocalDateSerializer;

@Entity
@Table(name="student_exams")
@NamedQueries({
    @NamedQuery(name = "StudentExams.findAll", query = "SELECT s FROM StudentExamEntity s")
    , @NamedQuery(name = "StudentExams.findById", query = "SELECT s FROM StudentExamEntity s WHERE s.id = :id")
    , @NamedQuery(name = "StudentExams.findByDate", query = "SELECT s FROM StudentExamEntity s WHERE s.date = :date"),
	@NamedQuery(name = "StudentExams.findAllCount", query = "SELECT COUNT(s) FROM StudentExamEntity s")})
public class StudentExamEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate date;

	@ManyToOne(optional = false)
	@JoinColumn(name="studentId")
	private StudentEntity student;

	@ManyToOne(optional = false)
	@JoinColumn(name="subjectProfessorId")
	private ExamEntity subjectProfessor;

	public StudentExamEntity() {
	}

	public StudentExamEntity(LocalDate date, StudentEntity student, ExamEntity subjectProfessor) {
		super();
		this.date = date;
		this.student = student;
		this.subjectProfessor = subjectProfessor;
	}

	public StudentExamEntity(Long id, LocalDate date, StudentEntity student, ExamEntity subjectProfessor) {
		super();
		this.id = id;
		this.date = date;
		this.student = student;
		this.subjectProfessor = subjectProfessor;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return this.date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public StudentEntity getStudent() {
		return this.student;
	}

	public void setStudent(StudentEntity student) {
		this.student = student;
	}

	public ExamEntity getSubjectProfessor() {
		return this.subjectProfessor;
	}

	public void setSubjectProfessor(ExamEntity subjectProfessor) {
		this.subjectProfessor = subjectProfessor;
	}

	@Override
	public String toString() {
		return "StudentExamEntity [id=" + id + ", date=" + date + ", student=" + student + ", subjectProfessor="
				+ subjectProfessor + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		result = prime * result + ((subjectProfessor == null) ? 0 : subjectProfessor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentExamEntity other = (StudentExamEntity) obj;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		if (subjectProfessor == null) {
			if (other.subjectProfessor != null)
				return false;
		} else if (!subjectProfessor.equals(other.subjectProfessor))
			return false;
		return true;
	}

}