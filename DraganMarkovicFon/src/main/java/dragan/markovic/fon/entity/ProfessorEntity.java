package dragan.markovic.fon.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import dragan.markovic.fon.util.LocalDateAttributeConverter;
import dragan.markovic.fon.util.LocalDateDeserializer;
import dragan.markovic.fon.util.LocalDateSerializer;

import dragan.markovic.fon.validator.Phone;


@Entity
@Table(name="professor")
@NamedQueries({
    @NamedQuery(name = "Professor.findAll", query = "SELECT p FROM ProfessorEntity p")
    , @NamedQuery(name = "Professor.findById", query = "SELECT p FROM ProfessorEntity p WHERE p.id = :id")
    , @NamedQuery(name = "Professor.findByEmail", query = "SELECT p FROM ProfessorEntity p WHERE p.email = :email")
    ,
	@NamedQuery(name = "Professor.findAllCount", query = "SELECT COUNT(p) FROM ProfessorEntity p")})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfessorEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@NotNull
	@Size(min=3, max=30)
	@Pattern(regexp="^[A-Za-z]*$")
	private String firstname;
	
	@NotNull
	@Size(min=3, max=30)
	@Pattern(regexp="^[A-Za-z]*$")
	private String lastname;
	@Size( max=60)
	private String address;
	@Size(max=30)
	@NotEmpty @Email
	private String email;
	@Phone
	private String phone;
	@DateTimeFormat(pattern="dd.MM.yyyy")
	@NotNull
	@PastOrPresent
	@Convert(converter = LocalDateAttributeConverter.class) 
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	
	private LocalDate reelectiondate;
	
	
	@ManyToOne(optional = false)
	@JoinColumn(name="titleId")
	private TitleEntity title;
	
	@JoinColumn(name="cityId")
	@ManyToOne(optional = true)
	private CityEntity city;

	public ProfessorEntity() {
	}

	public Long getId() {
		return this.id;
	}

	public String getAddress() {
		return address;
	}

	public void setId(Long id) {
		this.id = id;
	}
	

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	public LocalDate getReelectiondate() {
		return this.reelectiondate;
	}

	public void setReelectiondate(LocalDate reelectiondate) {
		this.reelectiondate = reelectiondate;
	}

	public TitleEntity getTitle() {
		return this.title;
	}

	public void setTitle(TitleEntity title) {
		this.title = title;
	}

	public CityEntity getCity() {
		return this.city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}


	@Override
	public String toString() {
		return "ProfessorEntity [id=" + id + ", address=" + address + ", email=" + email + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", phone=" + phone + ", reelectiondate=" + reelectiondate + ", title="
				+ title + ", city=" + city +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfessorEntity other = (ProfessorEntity) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}