package dragan.markovic.fon.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dragan.markovic.fon.model.StudentDto;
import dragan.markovic.fon.service.StudentService;

public class StudentDtoFormatter implements Formatter<StudentDto>{
	
	private  StudentService studentService;
	
	
	@Autowired
	public StudentDtoFormatter(StudentService studentServiceImpl) {

		this.studentService=studentServiceImpl;
	}

	@Override
	public String print(StudentDto studentDto, Locale locale) {
		System.out.println(studentDto);
		return studentDto.toString();
	}

	@Override
	public StudentDto parse(String t, Locale locale) {
		System.out.println(t);
		
		Long number=Long.parseLong(t);
		System.out.println(number);
		StudentDto studentDto = studentService.findById((long)number);
		return studentDto;
	}

}
