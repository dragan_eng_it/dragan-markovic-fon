package dragan.markovic.fon.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dragan.markovic.fon.model.SubjectDto;
import dragan.markovic.fon.service.SubjectService;

public class SubjectDtoFormatter implements Formatter<SubjectDto>{
	
	private  SubjectService subjectService;
	
	
	@Autowired
	public SubjectDtoFormatter(SubjectService subjectServiceImpl) {

		this.subjectService=subjectServiceImpl;
	}

	@Override
	public String print(SubjectDto subjectDto, Locale locale) {
		return subjectDto.toString();
	}

	@Override
	public SubjectDto parse(String t, Locale locale) {
		
		Long number=Long.parseLong(t);
		SubjectDto subjectDto = subjectService.findById((long)number);
		return subjectDto;
	}

}
