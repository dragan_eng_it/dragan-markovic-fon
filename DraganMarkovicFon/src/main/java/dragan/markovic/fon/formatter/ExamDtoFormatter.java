package dragan.markovic.fon.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dragan.markovic.fon.model.ExamDto;
import dragan.markovic.fon.service.ExamService;

public class ExamDtoFormatter implements Formatter<ExamDto>{
	
	private  ExamService examService;
	
	@Autowired
	public ExamDtoFormatter(ExamService examService) {
		this.examService=examService;
	}

	@Override
	public String print(ExamDto examDto, Locale locale) {
		return examDto.toString();
	}

	@Override
	public ExamDto parse(String t, Locale locale) {
		Long number=Long.parseLong(t);
		ExamDto examDto = examService.findById((long)number);
		return examDto;
	}

}
