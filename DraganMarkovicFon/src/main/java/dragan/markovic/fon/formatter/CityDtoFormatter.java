package dragan.markovic.fon.formatter;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dragan.markovic.fon.entity.CityEntity;
import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.repository.impl.CityRepository;

public class CityDtoFormatter implements Formatter<CityDto> {

	private final CityRepository cityRepository;

	@Autowired
	public CityDtoFormatter(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}

	@Override
	public String print(CityDto cityDto, Locale locale) {
		Optional<CityEntity> city = cityRepository.findById(4);
		if (city.isPresent()) {

		}
		return cityDto.toString();
	}

	@Override
	public CityDto parse(String id, Locale locale) {
		CityDto cityDto = new CityDto();
		Long br = Long.parseLong(id);
		Optional<CityEntity> city = cityRepository.findById(br);
		if (city.isPresent()) {
			cityDto.setId(br);
			cityDto.setName(city.get().getName());
			cityDto.setNumber(city.get().getNumber());

		}
		return cityDto;

	}
}
