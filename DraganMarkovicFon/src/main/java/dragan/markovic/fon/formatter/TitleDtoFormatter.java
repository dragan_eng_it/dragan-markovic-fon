package dragan.markovic.fon.formatter;

import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dragan.markovic.fon.entity.TitleEntity;
import dragan.markovic.fon.model.TitleDto;
import dragan.markovic.fon.repository.impl.TitleRepository;

public class TitleDtoFormatter implements Formatter<TitleDto>{
	
	private  TitleRepository titleRepository;
	
	
	@Autowired
	public TitleDtoFormatter(TitleRepository titleRepository) {
		this.titleRepository=titleRepository;
	}

	@Override
	public String print(TitleDto titleDto, Locale locale) {
		System.out.println(titleDto);
		return titleDto.toString();
	}

	@Override
	public TitleDto parse(String id, Locale locale) {
		Long number=Long.parseLong(id);
		//Optional<TitleEntity> titleDto = titleRepository.findById(number);
		return new TitleDto(number,"bez");
	}

}
