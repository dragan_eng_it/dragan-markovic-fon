package dragan.markovic.fon.formatter;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.model.ProfessorDto;
import dragan.markovic.fon.service.CityService;
import dragan.markovic.fon.service.ProfessorService;
import dragan.markovic.fon.service.TitleService;

public class ProfessorDtoFormatter implements Formatter<ProfessorDto>{
	
	
	private  ProfessorService professorService;
	private  CityService cityService;
	private  TitleService titleService;
	
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService,CityService cityService,TitleService titleService) {

		this.professorService=professorService;
		this.cityService=cityService;
		this.titleService=titleService;
	}

	
	@Override
	public String print(ProfessorDto professorDto, Locale locale) {
		System.out.println(professorDto);
		return professorDto.toString();
	}

	
	@Override
	public ProfessorDto parse(String t, Locale locale) {
		System.out.println(t);
		Long number=Long.parseLong(t);
		CityDto c = cityService.findById(number);
		return null;
	}

}


