package dragan.markovic.fon.repository;

import java.util.List;
import java.util.Optional;

public interface IRepository<T,U> {

	public List<T> getAll();
	public Optional<T> findById(long id);

	public void deleteById(Long id);
	public Optional<T> findByNamedQuery(String s);
	public int getToatalCount();
	public T update(T t);
	public T create(T t);
	public List<T> findByPage( int start,int number );
}
