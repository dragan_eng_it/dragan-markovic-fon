package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.CityEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class CityRepository implements IRepository<CityEntity, Long> {
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<CityEntity> getAll() {
		return em.createNamedQuery("City.findAll", CityEntity.class).getResultList();
	}

	@Override
	public Optional<CityEntity> findById(long id) {
		CityEntity cityEntity = em.find(CityEntity.class, id);
		return cityEntity != null ? Optional.of(cityEntity) : Optional.empty();
	}

	@Override
	public Optional<CityEntity> findByNamedQuery(String name) {
		List<CityEntity> cityEntity = em.createNamedQuery("City.findByName", CityEntity.class)
				.setParameter("name", name).getResultList();
		return cityEntity != null ? Optional.of(cityEntity.get(0)) : Optional.empty();
	}

	@Override
	
	public CityEntity create(CityEntity city) {
		if (city.getId() > 0) {
			throw new PersistenceException("vec postoji id " + city.getId());

		} else {
			city.setId(null);
			em.persist(city);
		}
		return city;
	}

	@Override
	@Transactional(readOnly = true)
	public CityEntity update(CityEntity city) {
		if (city.getId() > 0) {
			city = em.merge(city);
		} else {
			// insert
			throw new PersistenceException("ne postoji city id " + city.getId());
		}
		return city;
	}

	@Override
	@Transactional(readOnly = true)
	public void deleteById(Long id) {
		CityEntity city = em.find(CityEntity.class, id);
		if (city != null) {
			em.remove(city);
		}

	}

	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("City.findAllCount").getSingleResult()).intValue();


	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CityEntity> findByPage(int page, int size) {

		return em.createNamedQuery("City.findAll").setFirstResult((page-1)*size).setMaxResults(size).getResultList();
	}

}
