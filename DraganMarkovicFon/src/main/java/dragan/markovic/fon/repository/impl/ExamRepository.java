package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

import dragan.markovic.fon.entity.ExamEntity;
import dragan.markovic.fon.entity.ProfessorEntity;
import dragan.markovic.fon.entity.SubjectEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class ExamRepository implements IRepository<ExamEntity, Long> {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<ExamEntity> getAll() {
		return em.createQuery("from ExamEntity").getResultList();
	}

	@Override
	public Optional<ExamEntity> findById(final long id) {
		ExamEntity examEntity = em.find(ExamEntity.class, id);
		return examEntity != null ? Optional.of(examEntity) : Optional.empty();
	}

	public Optional<ExamEntity> findByNamedQuery(final String sDto) {
		ExamEntity examEntity = em.createNamedQuery("ExamEntity.findBySubject", ExamEntity.class)
				.setParameter("subject", sDto).getSingleResult();
		return examEntity != null ? Optional.of(examEntity) : Optional.empty();
	}

	@Override
	public ExamEntity create( ExamEntity exam) {
		if (exam.getId() > 0) {
			// update
			throw new PersistenceException("vec postoji ExamEntity id " + exam.getId());
		} else {
			// insert
			exam.setId(null);
			ProfessorEntity prof = em.find(ProfessorEntity.class, exam.getProfessor().getId());
			if (prof == null) {
				prof = em.merge(exam.getProfessor());
				exam.setProfessor(prof);
			} else {
				exam.setProfessor(prof);
			}
			SubjectEntity subj = em.find(SubjectEntity.class, exam.getSubject().getId());
			if (subj == null) {
				subj = em.merge(exam.getSubject());
				exam.setSubject(subj);
			} else {
				exam.setSubject(subj);
			}
			em.persist(exam);
			return exam;
		}
	}

	@Override
	public ExamEntity update(ExamEntity exam) {
		System.out.println(exam.toString());
		if (exam.getId() > 0) {
			// update
			ProfessorEntity prof = em.find(ProfessorEntity.class, exam.getProfessor().getId());
			if (prof == null) {
				prof = em.merge(exam.getProfessor());
				exam.setProfessor(prof);

			} else {
				exam.setProfessor(prof);
			}
			SubjectEntity subj = em.find(SubjectEntity.class, exam.getSubject().getId());
			if (subj == null) {
				subj = em.merge(exam.getSubject());
				exam.setSubject(subj);

			} else {
				exam.setSubject(subj);
			}
			em.merge(exam);
			return exam;
		} else {
			
			throw new PersistenceException("ne postoji id " + exam.getId());
		}
	}

	@Override
	public void deleteById(final Long id) {
		ExamEntity exam = em.find(ExamEntity.class, id);
		if (exam != null) {
			em.remove(exam);
		}

	}

	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("Exam.findAllCount").getSingleResult()).intValue();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ExamEntity> findByPage( int page, int size) {

		return em.createNamedQuery("Exam.findAll").setFirstResult((page - 1) * size)
				.setMaxResults(size).getResultList();
	}

}
