package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

import dragan.markovic.fon.entity.CityEntity;
import dragan.markovic.fon.entity.ProfessorEntity;
import dragan.markovic.fon.entity.TitleEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class ProfessorRepository implements IRepository<ProfessorEntity, Long> {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<ProfessorEntity> getAll() {
		return em.createQuery("from ProfessorEntity").getResultList();
	}

	@Override
	public Optional<ProfessorEntity> findById(final long id) {
		ProfessorEntity professorEntity = em.find(ProfessorEntity.class, id);
		return professorEntity != null ? Optional.of(professorEntity) : Optional.empty();
	}

	@Override
	public Optional<ProfessorEntity> findByNamedQuery(final String name) {
		ProfessorEntity professorEntity = em.createNamedQuery("ProfessorEntity.findByName", ProfessorEntity.class)
				.setParameter("name", name).getSingleResult();
		return professorEntity != null ? Optional.of(professorEntity) : Optional.empty();
	}

	@Override
	public ProfessorEntity create(ProfessorEntity p) {
		if (p.getId() > 0) {
			// update
			throw new PersistenceException("vec postoji id " + p.getId());
		} else {
			// insert
			p.setId(null);
			CityEntity dbCity = em.find(CityEntity.class, p.getCity().getId());
			if (dbCity == null) {
				dbCity = em.merge(p.getCity());
				p.setCity(dbCity);
				em.persist(p);
			} else {
				p.setCity(dbCity);
				em.persist(p);
			}
			return p;
		}
	}

	@Override
	public ProfessorEntity update(ProfessorEntity p) {
		if (p.getId() > 0) {
			// update
			CityEntity dbCity = em.find(CityEntity.class, p.getCity().getId());
			if (dbCity == null) {
				dbCity = em.merge(p.getCity());
				p.setCity(dbCity);
				TitleEntity dbTitle = em.find(TitleEntity.class, p.getTitle().getId());
				if (dbTitle == null) {
					dbTitle = em.merge(p.getTitle());
					p.setTitle(dbTitle);
					em.merge(p);
				} else {
					p.setTitle(dbTitle);
					em.merge(p);
				}
			} else {
				p.setCity(dbCity);
				em.merge(p);
			}
			return p;
		} else {
			// insert
			throw new PersistenceException("ne postoji id " + p.getId());
		}
	}

	@Override
	public void deleteById(final Long id) {
		ProfessorEntity professor = em.find(ProfessorEntity.class, id);
		if (professor != null) {
			em.remove(professor);
		}

	}

	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("Professor.findAllCount").getSingleResult()).intValue();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProfessorEntity> findByPage(int page, int size) {

		return  em.createNamedQuery("Professor.findAll").setFirstResult((page - 1) * size)
				.setMaxResults(size).getResultList();
	}

}
