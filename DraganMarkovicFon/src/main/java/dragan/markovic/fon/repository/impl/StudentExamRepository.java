package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

import dragan.markovic.fon.entity.ExamEntity;
import dragan.markovic.fon.entity.StudentEntity;
import dragan.markovic.fon.entity.StudentExamEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class StudentExamRepository implements IRepository<StudentExamEntity, Long> {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentExamEntity> getAll() {
		return em.createQuery("from StudentExamEntity").getResultList();
	}
	@Override
	public Optional<StudentExamEntity> findById(final long id) {
		StudentExamEntity studentExamEntity = em.find(StudentExamEntity.class, id);
		return studentExamEntity != null ? Optional.of(studentExamEntity) : Optional.empty();
	}
	
	@Override
	public Optional<StudentExamEntity> findByNamedQuery(final String name) {
		StudentExamEntity studentExamEntity = em.createNamedQuery("StudentExamEntity.findByName", StudentExamEntity.class)
				.setParameter("name", name).getSingleResult();
		return studentExamEntity != null ? Optional.of(studentExamEntity) : Optional.empty();
	}
	

	@Override
	public StudentExamEntity create(StudentExamEntity examOfStud) {
		if (examOfStud.getId() > 0) {
			// update
			throw new PersistenceException("vec postoji ExamEntity id " + examOfStud.getId());
		} else {
			// insert
			examOfStud.setId(null);
			StudentEntity student = em.find(StudentEntity.class, examOfStud.getStudent().getId());
			if (student == null) {
				student = em.merge(examOfStud.getStudent());
				examOfStud.setStudent(student);
			} else {
				examOfStud.setStudent(student);
			}
			ExamEntity exam = em.find(ExamEntity.class, examOfStud.getSubjectProfessor().getId());
			if (exam == null) {
				exam = em.merge(examOfStud.getSubjectProfessor());
				examOfStud.setSubjectProfessor(exam);
			} else {
				examOfStud.setSubjectProfessor(exam);
			}
			em.persist(examOfStud);
			return examOfStud;
		}
	}

	@Override
	public StudentExamEntity update(StudentExamEntity examOfStud) {
		if (examOfStud.getId() > 0) {
			// update
			StudentEntity student = em.find(StudentEntity.class, examOfStud.getStudent().getId());
			if (student == null) {
				student = em.merge(examOfStud.getStudent());
				examOfStud.setStudent(student);

			} else {
				examOfStud.setStudent(student);
			}
			ExamEntity exam = em.find(ExamEntity.class, examOfStud.getSubjectProfessor().getId());
			if (exam == null) {
				exam = em.merge(examOfStud.getSubjectProfessor());
				examOfStud.setSubjectProfessor(exam);

			} else {
				examOfStud.setSubjectProfessor(exam);
			}
			em.merge(examOfStud);
			return examOfStud;
		} else {
			
			throw new PersistenceException("ne postoji id " + examOfStud.getId());
		}
	}
	@Override
	public void deleteById(Long id) {
		StudentExamEntity studentExam = em.find(StudentExamEntity.class, id);
		if (studentExam != null) {
			em.remove(studentExam);
		}

	}
	@Override
	public int getToatalCount() {
		
		return  ((Number)em.createNamedQuery("StudentExam.findAllCount").getSingleResult()).intValue();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<StudentExamEntity> findByPage(final int  page,final int size) {
		return em.createNamedQuery("StudentExam.findAll").setFirstResult((page - 1) * size)
		.setMaxResults(size).getResultList();
	}

	

	
}

