package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.UserEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class UserRepository implements IRepository<UserEntity, Long> {
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<UserEntity> getAll() {
		return em.createQuery("from UserEntity").getResultList();
	}

	@Override
	public Optional<UserEntity> findById(final long id) {
		UserEntity userEntity = em.find(UserEntity.class, id);
		return userEntity != null ? Optional.of(userEntity) : Optional.empty();
	}

	@Override
	public Optional<UserEntity> findByNamedQuery(final String name) {
		UserEntity userEntity = em.createNamedQuery("UserEntity.findByName", UserEntity.class)
				.setParameter("name", name).getSingleResult();
		return userEntity != null ? Optional.of(userEntity) : Optional.empty();
	}

	@Override
	@Transactional(readOnly = true)
	public UserEntity create(UserEntity u) {
		if (u.getId() > 0) {
			throw new PersistenceException("vec postoji id " + u.getId());

		} else {
			em.persist(u);
		}
		return u;
	}

	@Override
	@Transactional(readOnly = true)
	public UserEntity update(UserEntity u) {
		if (u.getId() > 0) {
			u = em.merge(u);
		} else {
			// insert
			throw new PersistenceException("ne postoji city id " + u.getId());
		}
		return u;
	}

	@Override
	public void deleteById(final Long id) {
		UserEntity title = em.find(UserEntity.class, id);
		if (title != null) {
			em.remove(title);
		}

	}

	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("User.findAllCount").getSingleResult()).intValue();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserEntity> findByPage(final int page, final int size) {
		return em.createNamedQuery("User.findAll").setFirstResult((page - 1) * size).setMaxResults(size)
				.getResultList();
	}
}
