package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.SubjectEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class SubjectRepository implements IRepository<SubjectEntity, Long> {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectEntity> getAll() {
		return em.createQuery("from SubjectEntity").getResultList();
	}

	@Override
	public Optional<SubjectEntity> findById(long id) {
		SubjectEntity subjectEntity = em.find(SubjectEntity.class, id);
		return subjectEntity != null ? Optional.of(subjectEntity) : Optional.empty();
	}

	public Optional<SubjectEntity> findByNamedQuery(String name) {
		SubjectEntity subjectEntity = em.createNamedQuery("Subject.findByIndexNumber", SubjectEntity.class)
				.setParameter("name", name).getSingleResult();
		return subjectEntity != null ? Optional.of(subjectEntity) : Optional.empty();
	}

	@Override
	@Transactional(readOnly = true)
	public SubjectEntity create(SubjectEntity u) {
		if (u.getId() > 0) {
			throw new PersistenceException("vec postoji SubjectEntity id " + u.getId());

		} else {
			u.setId(null);
			em.persist(u);
		}
		return u;
	}

	@Override
	@Transactional(readOnly = true)
	public SubjectEntity update(SubjectEntity u) {
		if (u.getId() > 0) {
			u = em.merge(u);
		} else {
			// insert
			throw new PersistenceException("ne postoji SubjectEntity id " + u.getId());
		}
		return u;
	}
	@Override
	public void deleteById(final Long id) {
		SubjectEntity role = em.find(SubjectEntity.class, id);
		if (role != null) {
			em.remove(role);
		}else {
			// insert
			throw new PersistenceException("ne postoji SubjectEntity id " + id);
		}

	}

	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("Subject.findAllCount").getSingleResult()).intValue();


	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectEntity> findByPage(final int page,final int size) {
		return em.createNamedQuery("Subject.findAll").setFirstResult((page - 1) * size)
				.setMaxResults(size).getResultList();
	}

	

}
