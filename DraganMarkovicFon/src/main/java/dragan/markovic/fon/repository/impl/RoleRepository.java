package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.RoleEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class RoleRepository implements IRepository<RoleEntity, Long> {

	@PersistenceContext
	EntityManager em;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<RoleEntity> getAll() {
		return em.createQuery("from RoleEntity").getResultList();
	}
	@Override
	public Optional<RoleEntity> findById(final long id) {
		RoleEntity roleEntity = em.find(RoleEntity.class, id);
		return roleEntity != null ? Optional.of(roleEntity) : Optional.empty();
	}
	
	@Override
	public Optional<RoleEntity> findByNamedQuery(final String name) {
		RoleEntity roleEntity = em.createNamedQuery("RoleEntity.findByName", RoleEntity.class)
				.setParameter("name", name).getSingleResult();
		return roleEntity != null ? Optional.of(roleEntity) : Optional.empty();
	}
	

	@Override
	@Transactional(readOnly = true)
	public RoleEntity create(RoleEntity u) {
		if (u.getId() >0 ) {
			throw new PersistenceException("vec postoji id "+u.getId());
		
		} else {
			 em.persist(u);
		}
		return u;
	}
	@Override
	@Transactional(readOnly = true)
	public RoleEntity update(RoleEntity u) {
		if (u.getId() > 0) {
			u =	em.merge(u);
		} else {
			// insert
			throw new PersistenceException("ne postoji city id "+u.getId());
		}
		return u;
	}
	@Override
	public void deleteById(final Long id) {
		RoleEntity role = em.find(RoleEntity.class, id);
		if (role != null) {
			em.remove(role);
		}

	}
	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("Role.findAllCount").getSingleResult()).intValue();
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<RoleEntity> findByPage(final int  page,final int size) {
		return em.createNamedQuery("Role.findAll").setFirstResult((page - 1) * size)
		.setMaxResults(size).getResultList();
	}

	
}
