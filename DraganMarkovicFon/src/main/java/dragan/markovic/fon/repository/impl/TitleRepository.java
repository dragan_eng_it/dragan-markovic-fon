package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dragan.markovic.fon.entity.TitleEntity;
import dragan.markovic.fon.repository.IRepository;


@Repository
public class TitleRepository implements IRepository<TitleEntity, Long> {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<TitleEntity> getAll() {
		return em.createQuery("from TitleEntity").getResultList();
	}
	@Override
	public Optional<TitleEntity> findById(final long id) {
		TitleEntity titleEntity = em.find(TitleEntity.class, id);
		return titleEntity != null ? Optional.of(titleEntity) : Optional.empty();
	}
	
	@Override
	public Optional<TitleEntity> findByNamedQuery(final String name) {
		TitleEntity titleEntity = em.createNamedQuery("TitleEntity.findByName", TitleEntity.class)
				.setParameter("name", name).getSingleResult();
		return titleEntity != null ? Optional.of(titleEntity) : Optional.empty();
	}
	
	@Override
	@Transactional(readOnly = true)
	public TitleEntity create(TitleEntity t) {
		if (t.getId() >0 ) {
			throw new PersistenceException("vec postoji id "+t.getId());
		
		} else {
			 em.persist(t);
		}
		return t;
	}
	@Override
	@Transactional(readOnly = true)
	public TitleEntity update(TitleEntity t) {
		if (t.getId() > 0) {
			t =	em.merge(t);
		} else {
			// insert
			throw new PersistenceException("ne postoji city id "+t.getId());
		}
		return t;
	}

	@Override
	public void deleteById(Long id) {
		TitleEntity title = em.find(TitleEntity.class, id);
		if (title != null) {
			em.remove(title);
		}

	}
	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("Title.findAllCount").getSingleResult()).intValue();
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<TitleEntity> findByPage( final int page,final int size) {
		return em.createNamedQuery("Title.findAll").setFirstResult((page - 1) * size)
		.setMaxResults(size).getResultList();
	}

	

	
}

