package dragan.markovic.fon.repository.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

import dragan.markovic.fon.entity.CityEntity;
import dragan.markovic.fon.entity.StudentEntity;
import dragan.markovic.fon.repository.IRepository;

@Repository
public class StudentRepository implements IRepository<StudentEntity, Long> {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentEntity> getAll() {
		
		return em.createQuery("from StudentEntity").getResultList();
	}
	@Override
	public Optional<StudentEntity> findById(final long id) {
		StudentEntity studentEntity = em.find(StudentEntity.class, id);

		return studentEntity != null ? Optional.of(studentEntity) : Optional.empty();
	}
	@Override
	public Optional<StudentEntity> findByNamedQuery(final String indexnumber) {
		StudentEntity studentEntity = em.createNamedQuery("Student.findByIndexNumber", StudentEntity.class)
				.setParameter("indexnumber", indexnumber).getSingleResult();
		return studentEntity != null ? Optional.of(studentEntity) : Optional.empty();
	}
	
	@Override
	public StudentEntity create(StudentEntity student) {
		System.out.println(student.toString());
		if (student.getId()>0) {
			// update
			throw new PersistenceException("vec postoji id "+student.getId());
		}
		else {
			// insert
			student.setId(null);
			CityEntity dbCity=em.find(CityEntity.class,student.getCity().getId());
			if (dbCity==null) {
				dbCity = em.merge(student.getCity());
				student.setCity(dbCity);
				em.persist(student);
			}else {
				student.setCity(dbCity);
				em.persist(student);
			}
			return student;
		}
	}
	@Override
	public StudentEntity update(StudentEntity student) {
		if (student.getId()>0) {
			// update
			CityEntity dbCity=em.find(CityEntity.class,student.getCity().getId());
			if (dbCity==null) {
				dbCity = em.merge(student.getCity());
				student.setCity(dbCity);
				em.merge(student);
			}else {
				//student.setCity(dbCity);
				em.merge(student);
			}
			return student;
		}
		else {
			// insert
			throw new PersistenceException("ne postoji id "+student.getId());
		}
	}
	@Override
	public void deleteById(final Long id) {
		StudentEntity student = em.find(StudentEntity.class, id);
		if (student != null) {
			em.remove(student);
		}else {
			// insert
			throw new PersistenceException("ne postoji id "+id);
		}

	}
	@Override
	public int getToatalCount() {
		return  ((Number)em.createNamedQuery("Student.findAllCount").getSingleResult()).intValue();
	}
	@Override
	public List<StudentEntity> findByPage( final int page, final int size) {
		
		@SuppressWarnings("unchecked")
		List<StudentEntity> list = em.createNamedQuery("Student.findAll").setFirstResult((page-1)*size).setMaxResults(size).getResultList();
		
		return list;
	}

	

	
}

