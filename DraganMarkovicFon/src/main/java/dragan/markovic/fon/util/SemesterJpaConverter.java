package dragan.markovic.fon.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import dragan.markovic.fon.enums.Semester;

@Converter
public class SemesterJpaConverter implements AttributeConverter<Semester, String> {
 
   @Override
   public String convertToDatabaseColumn(Semester s) {
       if (s == null) {
           return null;
       }
       return s.toString();
   }
 
   @Override
   public Semester convertToEntityAttribute(String string) {
       if (string == null) {
           return null;
       }
       try {
           return Semester.valueOf(string);
       } catch (IllegalArgumentException e) {
           return null;
       }
   }
 
}