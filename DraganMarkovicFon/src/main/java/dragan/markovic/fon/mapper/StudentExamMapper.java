package dragan.markovic.fon.mapper;


import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.entity.StudentExamEntity;
import dragan.markovic.fon.model.StudentExamDto;

@Component("studentExamMapper")
public class StudentExamMapper extends  ModelMapper {
	
	
	private final ObjectMapper objectMapper=new ObjectMapper();
	public String toJson(StudentExamEntity ce) {
		String str;
		try {
			str = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(ce);
			return str;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error convert to json";
}
	public StudentExamDto toDto(String jsonInString) {
		StudentExamDto toDto=new StudentExamDto();
		try {
			
			toDto = objectMapper.readValue(jsonInString, StudentExamDto.class);
			System.out.println(toDto.toString());

			return toDto;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new StudentExamDto();
	}

}
