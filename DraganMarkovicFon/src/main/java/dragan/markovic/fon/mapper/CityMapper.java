package dragan.markovic.fon.mapper;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.model.CityDto;
import dragan.markovic.fon.entity.CityEntity;

@Component("cityMapper")
public class CityMapper extends ModelMapper  {
	private final ObjectMapper objectMapper = new ObjectMapper();
	private CityEntity city;
	private CityDto cityDto;
	private String strJson;

	public CityDto buildCityDto() {
		try {

			this.cityDto = objectMapper.readValue(this.strJson, CityDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.cityDto;
	}

	public CityEntity buildCity() {
		try {

			this.city = objectMapper.readValue(this.strJson, CityEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.city;
	}
public CityMapper makeJson(CityEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public CityMapper makeJson(CityDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public String getStrJson() {
		return strJson;
	}

	public void setStrJson(String strJson) {
		this.strJson = strJson;
	}

	
}
