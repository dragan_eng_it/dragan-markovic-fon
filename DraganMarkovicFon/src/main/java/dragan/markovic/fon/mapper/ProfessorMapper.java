package dragan.markovic.fon.mapper;


import java.io.IOException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import dragan.markovic.fon.entity.ProfessorEntity;
import dragan.markovic.fon.model.ProfessorDto;
import dragan.markovic.fon.model.ProfessorDtoForExam;

@Component("professorMapper")
public class ProfessorMapper extends ModelMapper{

	//private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	private ProfessorEntity professor;
	private ProfessorDto professorDto;
	private ProfessorDtoForExam professorDtoForExam;
	private String strJson;


	public ProfessorDto buildProfessorDto() {
		try {

			this.professorDto = objectMapper.readValue(this.strJson, ProfessorDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.professorDto;
	}
	public ProfessorDtoForExam buildProfessorDtoForExam() {
		try {

			this.professorDtoForExam = objectMapper.readValue(this.strJson, ProfessorDtoForExam.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.professorDtoForExam;
	}
	public ProfessorEntity buildProfessor() {
		try {

			this.professor = objectMapper.readValue(this.strJson, ProfessorEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.professor;
	}

	public ProfessorMapper makeJson(ProfessorEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public ProfessorMapper makeJson(ProfessorDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
	public ProfessorMapper makeJson(ProfessorDtoForExam c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}
