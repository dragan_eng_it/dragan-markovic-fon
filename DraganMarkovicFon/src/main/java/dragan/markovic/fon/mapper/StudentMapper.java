package dragan.markovic.fon.mapper;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.entity.StudentEntity;
import dragan.markovic.fon.model.StudentDto;

@Component("studentMapper")
public class StudentMapper extends ModelMapper {

	private final ObjectMapper objectMapper = new ObjectMapper();
	private StudentEntity student;
	private StudentDto studentDto;
	private String strJson;


	public StudentDto buildStudentDto() {
		try {
			
			this.studentDto = objectMapper.readValue(this.strJson, StudentDto.class);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.studentDto;
	}

	public StudentEntity buildStudent() {
		try {

			this.student = objectMapper.readValue(this.strJson, StudentEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.student;
	}

	public StudentMapper makeJson(StudentEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public StudentMapper makeJson(StudentDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}
