package dragan.markovic.fon.mapper;


import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.entity.TitleEntity;
import dragan.markovic.fon.model.TitleDto;

@Component("titleMapper")
public class TitleMapper extends  ModelMapper {
	
	private final ObjectMapper objectMapper = new ObjectMapper();
	private TitleEntity title;
	private TitleDto titleDto;
	private String strJson;

	public TitleDto buildTitleDto() {
		try {

			this.titleDto = objectMapper.readValue(this.strJson, TitleDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.titleDto;
	}

	public TitleEntity buildTitle() {
		try {

			this.title = objectMapper.readValue(this.strJson, TitleEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.title;
	}
public TitleMapper makeJson(TitleEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public TitleMapper makeJson(TitleDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}
