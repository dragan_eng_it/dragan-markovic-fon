package dragan.markovic.fon.mapper;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.entity.RoleEntity;
import dragan.markovic.fon.model.RoleDto;

@Component("roleMapper")
public class RoleMapper extends  ModelMapper {
	
	private final ObjectMapper objectMapper = new ObjectMapper();
	private RoleEntity role;
	private RoleDto roleDto;
	private String strJson;

	public RoleDto buildRoleDto() {
		try {

			this.roleDto = objectMapper.readValue(this.strJson, RoleDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.roleDto;
	}

	public RoleEntity buildRole() {
		try {

			this.role = objectMapper.readValue(this.strJson, RoleEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.role;
	}
public RoleMapper makeJson(RoleEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public RoleMapper makeJson(RoleDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}