package dragan.markovic.fon.mapper;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.entity.UserEntity;
import dragan.markovic.fon.model.UserDto;

@Component("userMapper")
public class UserMapper extends  ModelMapper {
	
	private final ObjectMapper objectMapper = new ObjectMapper();
	private UserEntity user;
	private UserDto userDto;
	private String strJson;

	public UserDto buildUserDto() {
		try {

			this.userDto = objectMapper.readValue(this.strJson, UserDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.userDto;
	}

	public UserEntity buildUser() {
		try {

			this.user = objectMapper.readValue(this.strJson, UserEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.user;
	}
public UserMapper makeJson(UserEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public UserMapper makeJson(UserDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}