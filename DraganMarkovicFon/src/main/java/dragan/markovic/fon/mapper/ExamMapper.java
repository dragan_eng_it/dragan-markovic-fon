package dragan.markovic.fon.mapper;

import java.io.IOException;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import dragan.markovic.fon.entity.ExamEntity;
import dragan.markovic.fon.model.ExamDto;

@Component("examMapper")
public class ExamMapper extends ModelMapper {

	private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
			false);

	private ExamEntity exam;
	private ExamDto examDto;
	private String strJson;

	public ExamDto buildExamDto() {
		try {

			this.examDto = objectMapper.readValue(this.strJson, ExamDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
System.out.println(this.examDto.toString());
		return this.examDto;
	}

	public ExamEntity buildExam() {
		try {

			this.exam = objectMapper.readValue(this.strJson, ExamEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.exam;
	}

	public ExamMapper makeJson(ExamEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			System.out.println(c);

			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public ExamMapper makeJson(ExamDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}