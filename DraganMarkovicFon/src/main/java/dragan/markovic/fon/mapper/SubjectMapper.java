package dragan.markovic.fon.mapper;


import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import dragan.markovic.fon.entity.SubjectEntity;
import dragan.markovic.fon.model.SubjectDto;

@Component("subjectMapper")
public class SubjectMapper extends  ModelMapper {
	
	private final ObjectMapper objectMapper = new ObjectMapper();
	private SubjectEntity subject;
	private SubjectDto subjectDto;
	private String strJson;

	public SubjectDto buildSubjectDto() {
		try {

			this.subjectDto = objectMapper.readValue(this.strJson, SubjectDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.subjectDto;
	}

	public SubjectEntity buildSubject() {
		try {

			this.subject = objectMapper.readValue(this.strJson, SubjectEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this.subject;
	}
public SubjectMapper makeJson(SubjectEntity c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}

	public SubjectMapper makeJson(SubjectDto c) {
		String str;
		try {
			str = objectMapper.writeValueAsString(c);
			this.strJson = str;
			return this;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return this;
	}
}